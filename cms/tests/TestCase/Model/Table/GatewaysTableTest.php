<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GatewaysTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GatewaysTable Test Case
 */
class GatewaysTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\GatewaysTable
     */
    protected $Gateways;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.Gateways',
        'app.Clients',
        'app.Measures',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Gateways') ? [] : ['className' => GatewaysTable::class];
        $this->Gateways = $this->getTableLocator()->get('Gateways', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Gateways);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\GatewaysTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\GatewaysTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
