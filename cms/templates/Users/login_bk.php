<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 * @var \Cake\Collection\CollectionInterface|string[] $roles
 * @var \Cake\Collection\CollectionInterface|string[] $clients
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="users form content">
            <?= $this->Form->create() ?>
            <fieldset>
                <legend><H2><?= __('User Login') ?></H2></legend>
                <legend><?= __('Please enter your email and password') ?></legend>
                <?php
                    echo $this->Form->control('email',['label'=>__('Email'),'placeholder'=>__('Insert email')]);
                    echo $this->Form->control('password',['label'=>__('Password'),'type'=>'password','placeholder'=>__('Insert password')]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Login')) ?>
            <?= $this->Form->end() ?>
            <?= $this->Html->link(__('Forgot Password'),['controller'=>'users','action'=>'forgotPassword']) ?>
        </div>
    </div>
</div>

