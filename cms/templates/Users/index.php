<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\user> $users
 */
?>

<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\User> $users
 * 
 * methods:
 * $this->block('nav');
   $this->block('menu');
   $this->block('path');
 */
    $this->element('home/headers'); 
?>

<?= $this->element('home/home_style_js_meta') ?>
<?= $this->element('Clients/list_style_js_meta') ?>

  <!-- entities list start -->
<section id="dashboard-admin" class="app-user-list">
  <!-- entities resume start-->
  
  <!-- list and filter start -->
  <div class="card">          

    <div class="card-body border-bottom">
      <h4 class="card-title"><?= __('Search &amp; Filter') ?></h4>
      <h3><?= $users->first()->client->name.' '.__($this->name) ?></h3>

      <div class="row">
        <div class="col-md-4 user_role">
          <label class="form-label" for="userType"><?= __('Role') ?></label>          

          <?php                
            echo $this->Form->select(
              'Role',
              $roles,
              [                                          
                'class'       => 'form-select text-capitalize mb-md-0 mb-2',
                'id'          => 'UserRole', 
                'placeholder' => ' ',
                'empty'  => 'Select Role',
              ]
            );
          ?>
        </div>
        <div class="col-md-4 user_plan">
          <label class="form-label" for="userPlan"><?= __('Status') ?></label>
          <?php
            $opt= [
                    '0' => 'Inactive',
                    '1' => 'Active',                      
                  ]; 
            echo $this->Form->select(
              'act',
              $opt,
              [                                          
                'class'       => 'form-select text-capitalize mb-md-0 mb-2',
                'id'          => 'UserAct', 
                'placeholder' => ' ',
                'empty'  => 'Select Status',
              ]
            );
           ?>
        </div>
        <div class="col-md-4 user_status">
          <label class="form-label" for="FilterTransaction"><?= __('Logged') ?></label>

          <?php
            $opt= [
                    '0' => 'Logout',
                    '1' => 'Login',                      
                  ]; 
            echo $this->Form->select(
              'act',
              $opt,
              [                                          
                'class'       => 'form-select text-capitalize mb-md-0 mb-2',
                'id'          => 'UserAct', 
                'placeholder' => ' ',
                'empty'  => 'Select Log Status',
              ]
            );
           ?>

        </div>
      </div>
    </div>

    <div class="card-datatable table-responsive pt-0">

      <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
        <div class="d-flex justify-content-between align-items-center header-actions mx-2 row mt-75">
          <div class="col-sm-12 col-lg-4 d-flex justify-content-center justify-content-lg-start">
            
          </div>
          <div class="col-sm-12 col-lg-8 ps-xl-75 ps-0">
            <div class="dt-action-buttons d-flex align-items-center justify-content-center justify-content-lg-end flex-lg-nowrap flex-wrap">

            <div class="me-1">
              <div id="DataTables_Table_0_filter" class="dataTables_filter">
                <label>Search:
                  <input type="search" class="form-control" placeholder="" aria-controls="DataTables_Table_0">
                </label>
              </div>
            </div>
            <div class="dt-buttons d-inline-flex mt-50">

              <button class="dt-button buttons-collection btn btn-outline-secondary dropdown-toggle me-2" tabindex="0" aria-controls="DataTables_Table_0" type="button" aria-haspopup="true"><span><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-external-link font-small-4 me-50"><path d="M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6"></path><polyline points="15 3 21 3 21 9"></polyline><line x1="10" y1="14" x2="21" y2="3"></line></svg><?= __('Export') ?></span>
              </button>

              <button class="dt-button add-new btn btn-primary" tabindex="0" aria-controls="DataTables_Table_0" type="button" data-bs-toggle="modal" data-bs-target="#modals-slide-in"><span><?= __('Add New '.$inflector->singularize($this->name)) ?></span>
              </button> 
              
            </div>
            </div>
          </div>
        </div>

        <table class="users-list-table table dataTable no-footer dtr-column" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="width: 1312px;">
          
          <thead class="table-light">
            <tr role="row">
              <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 347px" aria-label="Name: activate to sort column ascending"><?= $this->Paginator->sort(__('user')) ?></th>

              <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 150px" aria-label="Company: activate to sort column ascending"><?= $this->Paginator->sort(__('role_id')) ?></th>

              <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 110px;" aria-label="Role: activate to sort column ascending"><?= $this->Paginator->sort(__('status')) ?></th>

              <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 96px;" aria-label="state: activate to sort column ascending"><?= $this->Paginator->sort(__('logged')) ?></th>

              <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 96px;" aria-label="state: activate to sort column ascending"><?= $this->Paginator->sort(__('last active')) ?></th>


              <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 107px;" aria-label="Plan: activate to sort column ascending"><?= $this->Paginator->sort(__('modified')) ?></th>

              <th class="" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 107px;" aria-label="Plan: activate to sort column ascending"><?= $this->Paginator->sort(__('created')) ?></th>
              
              <th class=" actions" rowspan="1" colspan="1" style="width: 99px;" aria-label="Actions"><?= __('Actions') ?></th>

            </tr>
          </thead>

          <tbody>

          <?php foreach ($users as $user): ?>            
          <tr class="">
            <td class="sorting_1">
              <div class="d-flex justify-content-left align-items-center">
                <div class="avatar-wrapper">
                  <div class="avatar  me-1">
                    <img src="/img/Users/avatar/<?= $user->get('avatar') ?
                                                $user->get('avatar') : 'login.png'
                                                ?>" alt="Avatar" width="32" height="32">
                  </div>
                </div>
                <div class="d-flex flex-column">
                  <a href="
                    <?= $this->Url->build(
                        [
                          'controller'=>'users',
                          'action'=>'view',
                          $user->id,
                        ]
                      ) 
                    ?>
                  " class="user_name text-truncate text-body">
                    <span class="fw-bolder"><?= h($user->user) ?></span>
                  </a>
                  <small class="emp_post text-muted">
                    <?= $user->has('email') ? 
                        $this->Html->link(
                          h($user->email),
                          [
                            'controller' => 'Email',
                            'action'     => 'send',
                            $user->id,
                          ]
                        ) : ''
                    ?>                      
                  </small>
                </div>
              </div>
            </td>
            
            <td>
                <span class="text-truncate align-middle"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database font-medium-3 text-success me-50"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>
                <?= $user->has('role_id') ?
                    $this->Html->link(
                      h($user->role->type),
                      [
                        'controller' => 'CompanyTypes',
                        'action' => 'view',
                        $user->role->id,
                      ]
                    ) : ' ' ?></span>
            </td>
            <td>
              <span class="text-nowrap">
                  <?php
                      if( $user->has('act'))
                        echo ($user->act ? 'Active' : 'Inactive');
                      else
                        echo'';                    
                  ?>
              </span>
            </td>
            <td>
              <span class="text-nowrap">
                <?php
                      if( $user->has('logged'))
                        echo ($user->logged ? 'Login' : 'Logout');
                      else
                        echo'';                    
                ?>
              </span>
            </td>
            <td>
              <span class="text-nowrap">                
                <?= $user->has('last') ? $user->last : '' ?>
              </span>
            </td>
            <td>
              <span class="badge rounded-pill badge-light-success" text-capitalized="">
                <?= $user->has('modified') ? $user->modified : '' ?>
              </span>
            </td>
            <td>
              <span class="badge rounded-pill badge-light-success" text-capitalized=""> 
                <?= $user->has('created') ? $user->created : '' ?>
              </span>
            </td>
            <td>    
              <div class="btn-group">
                <a class="btn btn-sm dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical font-small-4"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg></a>

                <div class="dropdown-menu dropdown-menu-end">
                  
                  <a href="<?= $this->Url->build(['controller'=>'users','action'=>'view',$user->id]) ?>" class="dropdown-item"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text font-small-4 me-50"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>Details</a>

                  <a href="<?= $this->Url->build(['controller'=>'users','action' => 'delete', $user->id]) ?>" class="dropdown-item delete-record"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 font-small-4 me-50"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>Delete</a>

                </div>
              </div>
            </td>
          </tr> 
          <?php endforeach; ?> 

          </tbody>  
        </table>

        <!--  **** Paginator **** -->

        <div class="d-flex justify-content-between mx-2 row mb-1 paginator">
          <div class="col-sm-12 col-md-6">
            <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite"><p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p></div>
          </div>
          <div class="col-sm-12 col-md-6">
            <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
              <ul class="pagination">
                <li class="paginate_button page-item previous disabled" id="DataTables_Table_0_previous">
                  <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" class="page-link"><?= $this->Paginator->first('') ?></a>
                </li>
                <li class="paginate_button page-item previous disabled" id="DataTables_Table_0_previous">
                  <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" class="page-link"><?= $this->Paginator->prev('') ?></a>
                </li>
                <li class="paginate_button page-item active">
                  <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0" class="page-link"><?= $this->Paginator->counter('{{current}}') ?></a>
                </li>                
                <li class="paginate_button page-item next" id="DataTables_Table_0_next">
                  <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="6" tabindex="0" class="page-link"><?= $this->Paginator->next('') ?></a>
                </li>
                <li class="paginate_button page-item next" id="DataTables_Table_0_next">
                  <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="6" tabindex="0" class="page-link"><?= $this->Paginator->last('') ?></a>
                </li>
              </ul>
            </div>
          </div>
        </div>

      </div>

    </div>

    <!-- **** Modal to add new entity starts ***-->
    <div class="modal modal-slide-in new-user-modal fade" id="modals-slide-in">
      <div class="modal-dialog">
        <?= $this->Form->create(
            $user,
            [
              'url'=> [
                        'controller'=>'Users',
                        'action'=>'add',
                      ],
              'class'=>'add-new-user modal-content pt-0',
              'novalidate'=>'novalidate',
            ]
          ) 
        ?>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>

          <div class="modal-header mb-1">
            <h5 class="modal-title" id="exampleModalLabel">Add <?=  __($inflector->singularize($this->name)) ?></h5>
          </div>          

          <div class="modal-body flex-grow-1">
            

            <div class="mb-1">
              <label class="form-label" for="basic-icon-default-user">User Name</label>              
              <?= $this->Form->text(
                  'user',
                  [
                    'id'=>'basic-icon-default-user',
                    'class'=>'form-control',
                    'placeholder'=>'user2000',
                    'value'=> '',
                    'empty'=> '',
                  ]
                ) 
              ?>
            </div>

            <div class="mb-1">
            <label class="form-label" for="modalEditUserRole">Role</label>
            <div class="position-relative">            
              <?php
                $i = $this->Identity->get('role_id');
                foreach ($roles as $role){
                    if( 1 == $i ){
                      $roll[]= $i => $role;
                    }else if( $role->id > $i ){
                      $roll[]=$role;
                    }
                };
                pr($roll);
                echo $this->Form->select(
                  'role',
                  $roll, 
                  [                                          
                    'class'       => 'form-select',
                    'id'          => 'modalEditUserRole', 
                    'placeholder' => ' ',
                    'empty'       => 'Select Role',
                  ]
                );
              ?>
            </div>
            </div>

            <div class="mb-1">
              <label class="form-label" for="basic-icon-default-password">Password</label>
              <?= $this->Form->password(
                  'password',
                  [
                    'id'=>'basic-icon-default-password',
                    'class'=>'form-control',
                    'placeholder'=>'gorrinachiquero',
                    'value'=> '',
                  ]
                ) 
              ?>
            </div>            

            <div class="mb-1">
              <label class="form-label" for="basic-icon-default-email">Email</label>
              <?= $this->Form->text(
                  'email',
                  [
                    'type'=>'email',
                    'id'=>'basic-icon-default-email',
                    'class'=>'form-control dt-email',
                    'placeholder'=>'john.galts@who.is',
                    'value'=> '',
                  ]
                ) 
              ?>
            </div>

            <div class="mb-1">
              <label class="form-label" for="basic-icon-default-contact">Tel. Contact</label>
              <?= $this->Form->text(
                  'phone',
                  [
                    'id'=>'basic-icon-default-contact',
                    'class'=>'form-control dt-contact',
                    'placeholder'=>'+54 (11) 933-4422',
                    'value'=> '',
                  ]
                )
              ?>
            </div>

            <hr class="solid mb-2">

            <div class="mb-1">
              <label class="form-label" for="basic-icon-default-note">Comments</label>
              <?= $this->Form->textarea(
                  'note',
                  [
                    'id'=>'basic-icon-default-note',
                    'class'=>'form-control dt-note',
                    'placeholder'=>'Some things about this user',
                    'value'=> '',
                  ]
                )
              ?>
            </div>
                
            <?= $this->Form->button(
              __('Submit'),
              [
                'type'=> 'submit',
                'class'=>'btn btn-primary me-1 data-submit waves-effect waves-float waves-light',
              ]) ?> 
             
            <button type="reset" class="btn btn-outline-secondary waves-effect" data-bs-dismiss="modal"><?= __('Cancel') ?></button>

          </div>
        <?= $this->Form->end() ?>        
      </div>
    </div>
    <!-- Modal to add new user Ends-->
  <!-- Modal to add new user Ends-->
  </div>
  <!-- list and filter end -->
</section>
  <!-- users list ends -->
