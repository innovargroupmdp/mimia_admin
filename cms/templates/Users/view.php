<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Client> $clients
 */
?>

<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\User> $users
 * 
 * methods:
 * $this->block('nav');
   $this->block('menu');
   $this->block('path');
 */
 $this->element('home/headers'); 
?>

<?= $this->element('home/home_style_js_meta') ?>
<?= $this->element('Clients/list_style_js_meta') ?>
<?= $this->element('Clients/view/view_style_js_meta') ?>


<section class="app-user-view-account">
  <div class="row">
    <!-- User Sidebar -->
    <div class="col-xl-4 col-lg-5 col-md-5 order-1 order-md-0">
      <!-- User Card -->
      <div class="card">
        <div class="card-body">
          <div class="user-avatar-section">
            <div class="d-flex align-items-center flex-column">
              <img class="img-fluid rounded mt-3 mb-2" 
                src="/img/Users/avatar/<?= $user->get('avatar') ?
                                 $user->get('avatar') : 'login.png'
                                ?>"
                alt="Client Logo" width="110" height="110">
              <div class="user-info text-center">
                <h4><?= h($user->user) ?></h4>
                <span class="badge bg-light-secondary">
                  <?= $user->has('role_id') ?
                      $user->role->type : '' 
                  ?>
                </span>
              </div>
            </div>
          </div>
          <div class="d-flex justify-content-around my-2 pt-75">
            <div class="d-flex align-items-start me-2">
              <span class="badge bg-light-primary p-75 rounded">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check font-medium-2"><polyline points="20 6 9 17 4 12"></polyline></svg>
              </span>
              <div class="ms-75">
                <h4 class="mb-0">0</h4>
                <small>Operations Done</small>
              </div>
            </div>
            <div class="d-flex align-items-start">
              <span class="badge bg-light-primary p-75 rounded">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-briefcase font-medium-2"><rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect><path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path></svg>
              </span>
              <div class="ms-75">
                <h4 class="mb-0">568</h4>
                <small>Fixes Done</small>
              </div>
            </div>
          </div>
          <h4 class="fw-bolder border-bottom pb-50 mb-1"><?= __('Details') ?></h4>
          <div class="info-container">
            <ul class="list-unstyled">

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('User Name') ?>:</span>
                <span><?= h($user->user) ?></span>
              </li>

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Email') ?>:</span>
                <span><?= h($user->email) ?></span>
              </li>
                            
              <?php if ($user->has('phone')): ?>            

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Phone') ?>:</span>
                <span><?= h($user->phone) ?></span>
              </li>

              <?php endif; ?>

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Client Name') ?>:</span>
                <span><?=  $user->has('client_id') ? h($user->client->name) : '' ?></span>
              </li>

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Status') ?>:</span>
                <span class="badge bg-light-success"><?= ($user->act ? 'Active' : 'Inactive') ?></span>                
              </li>              

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Log Status') ?> :</span>
                <span class="badge bg-light-success"><?= h($user->logued) ?></span>
              </li>

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Last Login') ?> :</span>
                <span><?= h($user->last) ?></span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Modified') ?> :</span>
                <span><?= h($user->modified) ?></span>
              </li>
              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Created') ?> :</span>
                <span><?= h($user->created) ?></span>
              </li>
              
              <hr class="solid mb-2">

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Notes') ?>:</span>
                <span><?= $user->has('note') ? $user->note : '' ?></span>
              </li>
            </ul>
            <div class="d-flex justify-content-center pt-2">
              <a href="javascript:;" class="btn btn-primary me-1 waves-effect waves-float waves-light" data-bs-target="#editUser" data-bs-toggle="modal">
                Edit
              </a>
              <a href="javascript:;" class="btn btn-outline-danger suspend-user waves-effect">Suspended</a>
            </div>
          </div>
        </div>
      </div>
      <!-- /User Card -->
      <!-- Plan Card -->      
      <!-- /Plan Card -->
    </div>
    <!--/ User Sidebar -->

    <!-- User Content -->
    <div class="col-xl-8 col-lg-7 col-md-7 order-0 order-md-1">
      <!-- User Pills -->
      <ul class="nav nav-pills mb-2">
        <li class="nav-item">
          <a class="nav-link active" href="">
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user font-medium-3 me-50"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
            <span class="fw-bold">User Pills</span></a>
        </li>
      </ul>
      <!--/ User Pills -->

      <!-- Users table -->
      <div class="card">
        <h4 class="card-header">User View Pannel</h4>
        
      </div>
      <!-- /Users table -->

      <!-- Activity Timeline -->
      <div class="card">
        <h4 class="card-header">User Activity Timeline</h4>
        <div class="card-body pt-1">
          <ul class="timeline ms-50">
            
          </ul>
        </div>
      </div>
      <!-- /Activity Timeline -->

      <!-- Invoice table -->    
      <!-- /Invoice table -->
    </div>
    <!--/ User Content -->
  </div>
</section>

<!-- Edit User Modal -->
<div class="modal fade" id="editUser" tabindex="-1" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
    <div class="modal-content">
      <div class="modal-header bg-transparent">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body pb-5 px-sm-5 pt-50">
        <div class="text-center mb-2">
          <h1 class="mb-1">Edit User Information</h1>
          <p>Updating User details will receive a privacy audit.</p>
        </div>
        <div >

          <?= $this->Form->create(
              $user,
              [
               'url'=>[
                        'controller'=>'Users',
                        'action'=>'edit'
                      ],
                'id'=>'editUserForm',
                'class'=>'validate-form row gy-1 pt-75',
                'onsubmit'=>'return false',
                'novalidate'=>'novalidate',
                'enctype' => 'multipart/form-data',
              ]
            )
          ?> 
          <hr class="solid mb-0">

          <div class="col-12"> 
            <label class="form-label" for="avatar" ><?= __('Avatar Preview Image') ?></label>
          </div>  

          <div class="col-12 col-md-6">          
            <div class="d-flex align-items-center flex-column">              
            <a href="#" class="me-25">
              <img src="/img/Users/avatar/<?= $user->has('avatar') ?
                            $user->get('avatar') : 'login.png'
                        ?>" id="account-upload-img" class="uploadedAvatar rounded me-50" alt="profile image" width="100" height="100">
            </a>          
            </div>          
          </div>          

          <div class="col-12 col-md-6">
            <div>
            <!-- upload and reset button -->
              <label for="account-upload" class="btn btn-sm btn-primary mb-75 me-75 waves-effect waves-float waves-light">Upload</label>
              <?=  $this->Form->file(
                  'avatar',
                  [
                    'hidden'=> true,
                    'type'  => 'file',
                    'id'    => 'account-upload',
                    'accept'=> 'image/*',                  
                  ]
                )
              ?>          
              <button type="button" id="account-reset" class="btn btn-sm btn-outline-secondary mb-75 waves-effect">Reset</button>          
          <!--/ upload and reset button -->
              <p class="mb-0"><small>Allowed file types: png, jpg, jpeg.</small></p>
            </div>
          </div>
                

          <hr class="solid mb-2">

          <div class="col-12 col-md-6">

            <?= $this->Form->control(
                'f_name',
                [                
                'label'  => ['text' => __('First Name'),
                             'class' => 'form-label'],
                'class'  => 'form-control dt-full-name',
                'id'     => 'modalEditUserContact',
                'value'  => h($user->f_name),
                'placeholder' => __('John'),
                'data-msg' => "Please enter company contact full name"
                ]) ?>            
            
          </div>        
          <div class="col-12 col-md-6">

            <?= $this->Form->control(
                'l_name',
                [                
                'label'  => ['text' => __('Last Name'),
                             'class' => 'form-label'],
                'class'  => 'form-control dt-full-name',
                'id'     => 'modalEditUserContact',
                'value'  => h($user->l_name),
                'placeholder' => __('Galt'),
                'data-msg' => "Please enter company contact full name"
                ]) ?>            
            
          </div>        

          <div class="col-12 col-md-6">

            <?= $this->Form->control(
                'email',
                [                
                'label'  => ['text' => __('Email'),
                             'class' => 'form-label'],
                'type'   => 'email',            
                'class'  => 'form-control dt-email',
                'id'     => 'modalEditUserEmail',
                'value'  => h($user->email),
                'placeholder' => __('john.galt@who.is'),
                'data-msg' => "Please enter company contact email"
                ]) ?>

          </div>           

          <div class="col-12 col-md-6">
            <?= $this->Form->control(
                'phone',
                [                
                'label'  => ['text' => __('Phone'),
                             'class' => 'form-label'],
                'type'   => 'tel',             
                'class'  => 'form-control phone-number-mask',
                'id'     => 'modalEditUserPhone',
                'value'  => h($user->phone),
                'placeholder' => '+54 (11) 933-4422',
                'data-msg' => "Please enter company contact phone"
                ]) ?>               
          </div>         
          <div class="col-12">
            <div class="d-flex align-items-center mt-1">
              <div class="form-check form-switch form-check-primary">                
                <?= $this->Form->checkbox(
                                'act',
                                [
                                  'hiddenField' => false,
                                  'checked'=> $user->act? true:false,
                                  'class'  => 'form-check-input',
                                  'id'     => 'formAccountDeactivation',                                  
                                ],
                              ) 
                              ?>
                <label class="form-check-label" for="formAccountDeactivation">
                  <span class="switch-icon-left">
                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg>
                  </span>
                  <span class="switch-icon-right">
                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                  </span>
                </label>

              </div>
              <label class="form-check-label fw-bolder" for="formAccountDeactivation"><?= __('User Status') ?></label>
            </div>
          </div>
          <hr class="solid mb-2">          

          <div class="col-12">            
              <label class="form-label" for="basic-icon-default-note"><?= __('Comments') ?></label>
              <?= $this->Form->textarea(
                'note',
                [
                  'id'=>'basic-icon-default-contact',
                  'class'=>'form-control dt-note',
                  'value'  => h($user->note),
                  'placeholder'=>'Some things about tis user'
                ]) ?>
          </div>
          
          <div class="col-12 text-center mt-2 pt-50">
            <?= $this->Form->button(
              __('Submit'),
              [            
                  'type'=> 'submit',      
                  'class'=>'btn btn-primary me-1 waves-effect waves-float waves-light',
              ]) ?>
            
            <button type="reset" class="btn btn-outline-secondary waves-effect" data-bs-dismiss="modal" aria-label="Close">
              Discard
            </button>
          </div>
           <?= $this->Form->end() ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!--/ Edit User Modal -->

<!-- upgrade your plan Modal -->
<div class="modal fade" id="upgradePlanModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-upgrade-plan">
    <div class="modal-content">
      <div class="modal-header bg-transparent">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body px-5 pb-2">
        <div class="text-center mb-2">
          <h1 class="mb-1">Upgrade Plan</h1>
          <p>Choose the best plan for user.</p>
        </div>        
        <?= $this->Form->create(
              $client,
              [
               'url'=>[
                        'controller'=>'Clients',
                        'action'=>'edit'
                      ],
                'id'=>'upgradePlanForm',
                'class'=>'row pt-50',
                'onsubmit'=>'return false',
                'novalidate'=>'novalidate',
              ]
            )
          ?>
          <div class="col-sm-8">
            <?= $this->Form->control(
              'plan',
              [
                'opcions'=> $plans,
                'label'  => ['text' => __('Choose Plan'),
                             'class' => 'form-label'],
                'class'  => 'form-select',
                'id'     => 'choosePlan',
                'value'  => h($client->state_id),
                'placeholder' => 'Choose Plan',
                'data-msg' => "Please enter plan"
              ]) ?>                    

          </div>

          <div class="col-sm-4 text-sm-end">
            <?= $this->Form->button(
                __('Upgrade'),
                [            
                    'type'=> 'submit',      
                    'class'=>'btn btn-primary mt-2 waves-effect waves-float waves-light',
                ]
              ) 
            ?>
            
          </div>
        <?= $this->Form->end() ?>        
      </div>
      <hr>
      <div class="modal-body px-5 pb-3">
        <h6><?= __('User current plan is '.h($client->plan->type).' plan') ?></h6>
        <div class="d-flex justify-content-between align-items-center flex-wrap">
          <div class="d-flex justify-content-center me-1 mb-1">
            <sup class="h5 pricing-currency pt-1 text-primary">UVA</sup>
            <h1 class="fw-bolder display-4 mb-0 text-primary me-25"><?= h($client->plan->worth) ?></h1>
            <sub class="pricing-duration font-small-4 mt-auto mb-2">/ per plan unit </sub>
          </div>
          <?= $this->Form->postLink(
              'Cancel Subscription',
              ['action' => 'edit', $client->id],
              [ 
                'confirm' => 'Are you sure?',
                'data'=>'Cancel Subscription',
                'class'=>'btn btn-outline-danger cancel-subscription mb-1 waves-effect',
              ],              
            )
          ?>          
        </div>
      </div>
    </div>
  </div>
</div>
<!--/ upgrade your plan Modal -->
      