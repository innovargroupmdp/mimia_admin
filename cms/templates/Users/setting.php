<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 * @var string[]|\Cake\Collection\CollectionInterface $roles
 * @var string[]|\Cake\Collection\CollectionInterface $clients
 */
?>


<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\User> $users
 * 
 * methods:
 * $this->block('nav');
   $this->block('menu');
   $this->block('path');
 */
 $this->element('home/headers'); 
//$this->element('home/home_style_js_meta')
//$this->element('Clients/list_style_js_meta')
?>

<?= $this->element('Users/profile/profile_style_js_meta') ?>

<!-- profile -->
<div class="card">
  <div class="card-header border-bottom">
    <h4 class="card-title">Profile Details</h4>
  </div>
  <div class="card-body py-2 my-25">
    <!-- header section -->
    <div class="d-flex">
      <a href="#" class="me-25">
        <img src="/img/Users/avatar/<?= $user->has('avatar') ?
                            $user->get('avatar') : 'login.png'
                        ?>" id="account-upload-img" class="uploadedAvatar rounded me-50" alt="avatar image" width="100" height="100">
      </a>
      <!-- upload and reset button -->
      <div class="d-flex align-items-end mt-75 ms-1">
        <div>
          <label for="account-upload" class="btn btn-sm btn-primary mb-75 me-75 waves-effect waves-float waves-light">Upload</label>
          
          <?=  $this->Form->file(
                  'avatar',
                  [
                    'hidden'=> true,
                    'type'  => 'file',
                    'id'    => 'account-upload',
                    'accept'=> 'image/*',                  
                  ]
                )
              ?>          

          <button type="button" id="account-reset" class="btn btn-sm btn-outline-secondary mb-75 waves-effect">Reset</button>
      <!--/ upload and reset button -->

          <p class="mb-0">Allowed file types: png, jpg, jpeg.</p>
        </div>
      </div>
    </div>
    <!--/ header section -->

    <!-- form -->
      <?= $this->Form->create(
          $user,
          [
           'url'=>[
                    'controller'=>'Users',
                    'action'=>'setting'
                  ],
            'id'=>'editUserForm',
            'class'=>'validate-form mt-2 pt-50',
            'onsubmit'=>'return false',
            'novalidate'=>'novalidate',
            'enctype' => 'multipart/form-data',
          ]
        )
      ?>   
      <div class="row">

        <div class="col-12 col-sm-6 mb-1">

          <?= $this->Form->control(
            'user',
            [        
              'label'  => [
                            'text'  => __('User Name'),
                            'class' => 'form-label',
                            'for'   =>'accountUserName',
                          ],
              'class'  => 'form-control',
              'id'     => 'accountUserName',
              'value'  => h($user->user),
              'placeholder' => __('User2000'),
              'data-msg' => "Please enter User name"
            ])
          ?>                              
        </div>

        <div class="col-12 col-sm-6 mb-1"> 
        <label class="form-label" for="accountPassword"><?= __('Password') ?></label>   
        <?= $this->Form->password(
          'password',
          [                            
            'type'   => 'password',            
            'class'  => 'form-control',
            'id'     => 'accountPassword',
            'value'  =>  h($user->password),
            'placeholder' => __('Insert password'),
            'data-msg' => "Please enter password"
          ])
        ?>                              
        </div>
                
        <div class="col-12 col-sm-6 mb-1">
        <?= $this->Form->control(
          'f_Name',
          [                
            'label'  => [
                          'text'  => __('First Name'),
                          'class' => 'form-label',
                          'for'   =>'accountFirstName',
                        ],
            'class'  => 'form-control',
            'id'     => 'accountFirstName',
            'value'  => h($user->f_name),
            'placeholder' => __('John'),
            'data-msg' => "Please enter first name"
          ])
        ?>                              
        </div>

        <div class="col-12 col-sm-6 mb-1">
          <?= $this->Form->control(
          'l_Name',
          [                
            'label'  => [
                          'text'  => __('Last Name'),
                          'class' => 'form-label',
                          'for'   =>'accountLastName',
                        ],
            'class'  => 'form-control',
            'id'     => 'accountLastName',
            'value'  => h($user->l_name),
            'placeholder' => __('Glat'),
            'data-msg' => "Please enter last name"
          ])
        ?>
        </div>

        <div class="col-12 col-sm-6 mb-1">

          <?= $this->Form->control(
            'email',
            [                
            'label'  => ['text'  => __('Email'),
                         'class' => 'form-label',
                         'for'   =>'accountEmail',
                        ],
            'type'   => 'email',            
            'class'  => 'form-control dt-email',
            'id'     => 'accountEmail',
            'value'  => h($user->email),
            'placeholder' => __('john.galt@who.is'),
            'data-msg' => "Please enter company contact email"
            ]) ?>
          
        </div>
        
        <div class="col-12 col-sm-6 mb-1">
           <?= $this->Form->control(
            'phone',
            [                
            'label'  => ['text'  => __('Phone'),
                         'class' => 'form-label',
                         'for'   =>'accountPhoneNumber',
                        ],
            'type'   => 'tel',             
            'class'  => 'form-control account-number-mask',
            'id'     => 'accountPhoneNumber',
            'value'  => h($user->phone),
            'placeholder' => '+54 (11) 933-4422',
            'data-msg' => "Please enter company contact phone"
            ]) ?>                      
        </div>
                
        <div class="col-12">     

        <?= $this->Form->button(
          __('Save changes'),
          [            
            'type'=> 'submit',      
            'class'=>'btn btn-primary mt-1 me-1 waves-effect waves-float waves-light',
          ]) 
        ?>
          <button type="reset" class="btn btn-outline-secondary mt-1 waves-effect">Discard</button>

        </div>
      </div>
    <?= $this->Form->end() ?>
    <!--/ form -->
  </div>
</div>