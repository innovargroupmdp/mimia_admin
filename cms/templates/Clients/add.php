<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Client $client
 * @var \Cake\Collection\CollectionInterface|string[] $companyTypes
 * @var \Cake\Collection\CollectionInterface|string[] $plans
 * @var \Cake\Collection\CollectionInterface|string[] $billings
 * @var \Cake\Collection\CollectionInterface|string[] $states
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Clients'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="clients form content">
            <?= $this->Form->create($client) ?>
            <fieldset>
                <legend><?= __('Add Client') ?></legend>
                <?php
                    echo $this->Form->control('client_tag');
                    echo $this->Form->control('name');
                    echo $this->Form->control('vat');
                    echo $this->Form->control('company_type_id', ['options' => $companyTypes]);
                    echo $this->Form->control('web');
                    echo $this->Form->control('address');
                    echo $this->Form->control('country');
                    echo $this->Form->control('plan_id', ['options' => $plans, 'empty' => true]);
                    echo $this->Form->control('billing_id', ['options' => $billings, 'empty' => true]);
                    echo $this->Form->control('user');
                    echo $this->Form->control('password');
                    echo $this->Form->control('state_id', ['options' => $states]);
                    echo $this->Form->control('contact');
                    echo $this->Form->control('email');
                    echo $this->Form->control('phone');
                    echo $this->Form->control('act');
                    echo $this->Form->control('enroll');
                    echo $this->Form->control('down', ['empty' => true]);
                    echo $this->Form->control('logo');
                    echo $this->Form->control('note');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
