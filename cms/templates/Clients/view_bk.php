<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Client $client
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Client'), ['action' => 'edit', $client->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Client'), ['action' => 'delete', $client->id], ['confirm' => __('Are you sure you want to delete # {0}?', $client->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Clients'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Client'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="clients view content">
            <h3><?= h($client->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($client->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Vat') ?></th>
                    <td><?= h($client->vat) ?></td>
                </tr>
                <tr>
                    <th><?= __('Web') ?></th>
                    <td><?= h($client->web) ?></td>
                </tr>
                <tr>
                    <th><?= __('Address') ?></th>
                    <td><?= h($client->address) ?></td>
                </tr>
                <tr>
                    <th><?= __('Country') ?></th>
                    <td><?= h($client->country) ?></td>
                </tr>
                <tr>
                    <th><?= __('Plan') ?></th>
                    <td><?= $client->has('plan') ? $this->Html->link($client->plan->id, ['controller' => 'Plans', 'action' => 'view', $client->plan->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Billing') ?></th>
                    <td><?= $client->has('billing') ? $this->Html->link($client->billing->id, ['controller' => 'Billings', 'action' => 'view', $client->billing->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= h($client->user) ?></td>
                </tr>
                <tr>
                    <th><?= __('State') ?></th>
                    <td><?= $client->has('state') ? $this->Html->link($client->state->id, ['controller' => 'States', 'action' => 'view', $client->state->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Contact') ?></th>
                    <td><?= h($client->contact) ?></td>
                </tr>
                <tr>
                    <th><?= __('Email') ?></th>
                    <td><?= h($client->email) ?></td>
                </tr>
                <tr>
                    <th><?= __('Phone') ?></th>
                    <td><?= h($client->phone) ?></td>
                </tr>
                <tr>
                    <th><?= __('Logo') ?></th>
                    <td><?= h($client->logo) ?></td>
                </tr>
                <tr>
                    <th><?= __('Note') ?></th>
                    <td><?= h($client->note) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($client->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Client Tag') ?></th>
                    <td><?= $this->Number->format($client->client_tag) ?></td>
                </tr>
                <tr>
                    <th><?= __('Enroll') ?></th>
                    <td><?= h($client->enroll) ?></td>
                </tr>
                <tr>
                    <th><?= __('Down') ?></th>
                    <td><?= h($client->down) ?></td>
                </tr>
                <tr>
                    <th><?= __('Company Type') ?></th>
                    <td><?= $client->company_type->id ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th><?= __('Act') ?></th>
                    <td><?= $client->act ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Gateways') ?></h4>
                <?php if (!empty($client->gateways)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Gw Tag') ?></th>
                            <th><?= __('Gw Mac') ?></th>
                            <th><?= __('Mac') ?></th>
                            <th><?= __('Key Svr0') ?></th>
                            <th><?= __('Key Svr') ?></th>
                            <th><?= __('Boot') ?></th>
                            <th><?= __('Boot Init') ?></th>
                            <th><?= __('Release Version') ?></th>
                            <th><?= __('Act') ?></th>
                            <th><?= __('On Service') ?></th>
                            <th><?= __('Off Service') ?></th>
                            <th><?= __('Client Id') ?></th>
                            <th><?= __('Comment') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($client->gateways as $gateways) : ?>
                        <tr>
                            <td><?= h($gateways->id) ?></td>
                            <td><?= h($gateways->gw_tag) ?></td>
                            <td><?= h($gateways->gw_mac) ?></td>
                            <td><?= h($gateways->mac) ?></td>
                            <td><?= h($gateways->key_svr0) ?></td>
                            <td><?= h($gateways->key_svr) ?></td>
                            <td><?= h($gateways->boot) ?></td>
                            <td><?= h($gateways->boot_Init) ?></td>
                            <td><?= h($gateways->release_version) ?></td>
                            <td><?= h($gateways->act) ?></td>
                            <td><?= h($gateways->on_service) ?></td>
                            <td><?= h($gateways->off_service) ?></td>
                            <td><?= h($gateways->client_id) ?></td>
                            <td><?= h($gateways->comment) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Gateways', 'action' => 'view', $gateways->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Gateways', 'action' => 'edit', $gateways->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Gateways', 'action' => 'delete', $gateways->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gateways->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Mimias') ?></h4>
                <?php if (!empty($client->mimias)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Mimia Tag') ?></th>
                            <th><?= __('Mm Mac') ?></th>
                            <th><?= __('Mac') ?></th>
                            <th><?= __('Mm Tk 0') ?></th>
                            <th><?= __('Slot') ?></th>
                            <th><?= __('Boot') ?></th>
                            <th><?= __('Boot Init') ?></th>
                            <th><?= __('Release Version') ?></th>
                            <th><?= __('Act') ?></th>
                            <th><?= __('On Service') ?></th>
                            <th><?= __('Off Service') ?></th>
                            <th><?= __('Client Id') ?></th>
                            <th><?= __('Comment') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($client->mimias as $mimias) : ?>
                        <tr>
                            <td><?= h($mimias->id) ?></td>
                            <td><?= h($mimias->mimia_tag) ?></td>
                            <td><?= h($mimias->mm_mac) ?></td>
                            <td><?= h($mimias->mac) ?></td>
                            <td><?= h($mimias->mm_tk_0) ?></td>
                            <td><?= h($mimias->slot) ?></td>
                            <td><?= h($mimias->boot) ?></td>
                            <td><?= h($mimias->boot_init) ?></td>
                            <td><?= h($mimias->release_version) ?></td>
                            <td><?= h($mimias->act) ?></td>
                            <td><?= h($mimias->on_service) ?></td>
                            <td><?= h($mimias->off_service) ?></td>
                            <td><?= h($mimias->client_id) ?></td>
                            <td><?= h($mimias->comment) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Mimias', 'action' => 'view', $mimias->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Mimias', 'action' => 'edit', $mimias->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Mimias', 'action' => 'delete', $mimias->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mimias->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Users') ?></h4>
                <?php if (!empty($client->users)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('User') ?></th>
                            <th><?= __('Email') ?></th>
                            <th><?= __('Password') ?></th>
                            <th><?= __('Role Id') ?></th>
                            <th><?= __('Client Id') ?></th>
                            <th><?= __('Avatar') ?></th>
                            <th><?= __('Act') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($client->users as $users) : ?>
                        <tr>
                            <td><?= h($users->id) ?></td>
                            <td><?= h($users->user) ?></td>
                            <td><?= h($users->email) ?></td>
                            <td><?= h($users->password) ?></td>
                            <td><?= h($users->role_id) ?></td>
                            <td><?= h($users->client_id) ?></td>
                            <td><?= h($users->avatar) ?></td>
                            <td><?= h($users->act) ?></td>
                            <td><?= h($users->created) ?></td>
                            <td><?= h($users->modified) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
