<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Client> $clients
 */
?>

<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\User> $users
 * 
 * methods:
 * $this->block('nav');
   $this->block('menu');
   $this->block('path');
 */
 $this->element('home/headers'); 
?>

<?= $this->element('home/home_style_js_meta') ?>
<?= $this->element('Clients/list_style_js_meta') ?>
<?= $this->element('Clients/view/view_style_js_meta') ?>

<section class="app-user-view-account">
  <div class="row">
    <!-- User Sidebar -->
    <div class="col-xl-4 col-lg-5 col-md-5 order-1 order-md-0">
      <!-- User Card -->
      <div class="card">
        <div class="card-body">
          <div class="user-avatar-section">
            <div class="d-flex align-items-center flex-column">
              <img class="img-fluid rounded mt-3 mb-2" 
                src="/img/Clients/logo/<?= $client->get('logo') ?
                                        $client->get('logo') : 'acme.jpeg'
                                        ?>"
                alt="Client Logo" width="110" height="110">
              <div class="user-info text-center">
                <h4><?= h($client->name) ?></h4>
                <span class="badge bg-light-secondary">
                  <?= $client->has('company_type') ?
                      $client->company_type->type : '' 
                  ?>
                </span>
              </div>
            </div>
          </div>
          <div class="d-flex justify-content-around my-2 pt-75">
            <div class="d-flex align-items-start me-2">
              <span class="badge bg-light-primary p-75 rounded">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check font-medium-2"><polyline points="20 6 9 17 4 12"></polyline></svg>
              </span>
              <div class="ms-75">
                <h4 class="mb-0">1.23k</h4>
                <small>Tasks Done</small>
              </div>
            </div>
            <div class="d-flex align-items-start">
              <span class="badge bg-light-primary p-75 rounded">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-briefcase font-medium-2"><rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect><path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path></svg>
              </span>
              <div class="ms-75">
                <h4 class="mb-0">568</h4>
                <small>Projects Done</small>
              </div>
            </div>
          </div>
          <h4 class="fw-bolder border-bottom pb-50 mb-1"><?= __('Details') ?></h4>
          <div class="info-container">
            <ul class="list-unstyled">

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('User Name') ?>:</span>
                <span><?= h($client->user) ?></span>
              </li>

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Billing Email') ?>:</span>
                <span><?= h($client->email) ?></span>
              </li>

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Status') ?>:</span>
                <span class="badge bg-light-success"><?= h($client->state->type) ?></span>
              </li>

              <li class="mb-75">
                <span class="fw-bolder me-25">Web:</span>
                <span><?= $this->Html->link(
                          h($client->web),
                          'https://'.h($client->web)
                        ) 
                      ?>                        
                </span>
              </li>

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Address') ?>:</span>
                <span><?= h($client->address) ?></span>
              </li>

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Tax') ?> ID:</span>
                <span><?= h($client->vat) ?></span>
              </li>

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Enrolled') ?> :</span>
                <span><?= h($client->enroll) ?></span>
              </li>
              
              <hr class="solid mb-2">

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Contact Name') ?>:</span>
                <span><?= h($client->contact) ?></span>
              </li>

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Phone') ?>:</span>
                <span><?= h($client->phone) ?></span>
              </li>
                          
              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Country') ?>:</span>
                <span><?= h($client->country) ?></span>
              </li>

              <li class="mb-75">
                <span class="fw-bolder me-25"><?= __('Notes') ?>:</span>
                <span><?= $client->has('note') ? $client->note : '' ?></span>
              </li>
            </ul>
            <div class="d-flex justify-content-center pt-2">
              <a href="javascript:;" class="btn btn-primary me-1 waves-effect waves-float waves-light" data-bs-target="#editUser" data-bs-toggle="modal">
                Edit
              </a>
              <a href="javascript:;" class="btn btn-outline-danger suspend-user waves-effect">Suspended</a>
            </div>
          </div>
        </div>
      </div>
      <!-- /User Card -->
      <!-- Plan Card -->
      <div class="card border-primary">
        <div class="card-body">
          <div class="d-flex justify-content-between align-items-start">
            <span class="badge bg-light-primary"><?= __(h($client->plan->type)) ?></span>
            <div class="d-flex justify-content-center">
              <sup class="h5 pricing-currency text-primary mt-1 mb-0">UVA</sup>
              <span class="fw-bolder display-5 mb-0 text-primary"><?= h($client->plan->worth) ?></span>
              <sub class="pricing-duration font-small-4 ms-25 mt-auto mb-2">/<?= __(' per plan unit') ?></sub>
            </div>
          </div>
          <h6> Plan Unit :</h6>
          <ul class="ps-1 mb-2">
            <li class="mb-50"><? $client->plan->api ?> Users API</li>
            <li class="mb-50"><? $client->plan->portal ?> Users Portal</li>
            <li class="mb-50"><? $client->plan->mimias ?> MIMIAS</li>
            <li class="mb-50"><? $client->plan->gateways ?> Gateways</li>            
            <li>Basic Support</li>
          </ul>
          <div class="d-flex justify-content-between align-items-center fw-bolder mb-50">
            <span>Days</span>
            <span>4 of 30 Days</span>
          </div>
          <div class="progress mb-50" style="height: 8px">
            <div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="65" aria-valuemax="100" aria-valuemin="80"></div>
          </div>
          <span>4 days remaining</span>
          <div class="d-grid w-100 mt-2">
            <button class="btn btn-primary waves-effect waves-float waves-light" data-bs-target="#upgradePlanModal" data-bs-toggle="modal">
              Upgrade Plan
            </button>
          </div>
        </div>
      </div>
      <!-- /Plan Card -->
    </div>
    <!--/ User Sidebar -->

    <!-- User Content -->
    <div class="col-xl-8 col-lg-7 col-md-7 order-0 order-md-1">
      <!-- User Pills -->
      <ul class="nav nav-pills mb-2">
        <li class="nav-item">
          <a class="nav-link active" href="<? $this->Url->build([
                                              'controller'=>'Clients',
                                              'action'=>'view',
                                              $client->id,
                                              ]) ?>
            ">
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user font-medium-3 me-50"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
            <span class="fw-bold">Account</span></a>
        </li>

        <li class="nav-item">
          <a class="nav-link " href="<?= $this->Url->build([
                                         'controller'=>'Users',
                                         'action'=>'index',
                                          $client->id,
                                         ]) ?>
            ">
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user font-medium-3 me-50"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user font-medium-3 me-50"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
            <span class="fw-bold">Users</span></a>
        </li>

        <li class="nav-item">
          <a class="nav-link " href="<?= $this->Url->build([
                                         'controller'=>'Gateways',
                                         'action'=>'index',
                                          $client->id,
                                         ]) ?>
            ">            
             <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user font-medium-3 me-50"><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg>
             <span class="fw-bold"> Gateways</span></a>
        </li>

        <li class="nav-item">
          <a class="nav-link " href="<?= $this->Url->build([
                                         'controller'=>'Mimias',
                                         'action'=>'index',
                                          $client->id,
                                         ]) ?>
            ">            
             <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="20" viewBox="0 0 24 46.08" class="feather feather-user font-medium-3 me-50">
              <image width="23.467" height="46.08" preserveAspectRatio="none" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADcAAABsCAYAAADOkipyAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9 kT1Iw0AcxV9TpaIVh3YQccjQOlkRFXHUKhShQqgVWnUwufQLmjQkKS6OgmvBwY/FqoOLs64OroIg +AHi6OSk6CIl/q8ptIjx4Lgf7+497t4BQr3MNKtrHNB020wl4mImuyoGXtGHEIAxRGVmGXOSlITn +LqHj693MZ7lfe7P0a/mLAb4ROJZZpg28Qbx9KZtcN4nDrOirBKfE4+adEHiR64rLr9xLjRZ4Jlh M52aJw4Ti4UOVjqYFU2NeIo4omo65QsZl1XOW5y1cpW17slfGMzpK8tcpzmMBBaxBAkiFFRRQhk2 YrTqpFhI0X7cwz/U9EvkUshVAiPHAirQIDf94H/wu1srPznhJgXjQPeL43xEgcAu0Kg5zvex4zRO AP8zcKW3/ZU6MPNJeq2tRY6AgW3g4rqtKXvA5Q4w+GTIptyU/DSFfB54P6NvygKhW6B3ze2ttY/T ByBNXSVvgINDYKRA2ese7+7p7O3fM63+fgCJgHKwbeojhQAAAAZiS0dEAP8A/wD/oL2nkwAAAAlw SFlzAAAN1wAADdcBQiibeAAAAAd0SU1FB+YLEwEAHCx2rScAAAPeSURBVHja7Zw/jBVVFId/57FL ghAQKAgLyp/CgJBgoCMhFLaQ2NqYSKKtjQUUxEQbE60ksaZBKwKxIwQSCIYQAoJGMEZ3G2Nwn8zs Lq5PXNjPYsdiX+bNzOPNzN6ZnNPtzrvnnm9+5565c+/MSC02q6MTYK2k7ZLWSfpT0u9mttDYswaM Ae8C14AFlttfwDfAW4A1DWwf8IBidhd4vSlgR4BZhrN54FjoYK8Af/Bi1gOOhAx3kdHsV+ClEMEO Aos5wT8vAHiqrJg6JfK9l3FpuSRpj6TVkg5L+jHDz4ngKigwNUCJSWBN3293A/9mqLcvGOWAnZJ2 Djh82cx6y2YOZpOSvs9wuT+ktNyRcWzjkP/P81c73ETGsWPA/j6l35a0O6PN6jKCGisJbm3GsTWS bgBfSPo5KSjv5/h7EhJcN+f4Bkmnh/D3OKS0nC65+MYhwXVLhotcuTrgzGxOUq+tyim5w27lmCsz NZ+UtQQRIlxcVkAhwkUhwnUdztOyvXCxw3laerUsNL/slXSTGWRalpWaUahwXVeuocqNCvespHEb JFxsZrR1zMVlBhMaXBQy3HSblZt25RoK15VEKGk5cK8A6Eg6KumN5CTclXTNzBYz5pfPgFjSpirg gDFJb2pp/25R0i1JN4e6fAB7gO9SdjzvAXtz2j4cYcP/nQy/h4CfUtp8C+woCrYNeJQRwCNge0b7 6yPAHR/g8zVgJqPdJLCpyJj7WNKWDP4tkj6pqKgMKiifaWkbbJDtknQyT7VOwad/ZoFVA3x8OYJy e1P8rct5OOB/m8pTboOk9QXO8HpJmytQLq2gTEgaL9D21f4T3g83PkQg4xVMweIh+kkTalWV17lR lJs3s6chX8RHUS4qO5CQlIvbDNcI5aJkuaB9yiVzz8dtVe5FUzN2uADgup6WDYXr+pjzaukFJWjl nkuaayvcTNaqWlBwZjYr6elKjrcqlRt23EVNg5t2OE9LVy5Y5eI2K+fV0secV8t6lZtpFJyZ/V0w aBoHl9jNAr/5wczmmwj3ufKfbvi0qs4rhTOzq5I+UPryOpI+MrOvq+p/rGLlZGZngCuSTkg6kPR5 T9JZM7tfZd+VwyWADyR9qJqtoxabwzmcwzmcwzmcwzmcwzmcwznccvtniLa9mmIsGtNC/x1/p++m ck7F1hsjM4tqgvtNxXZpp/q3ntPS8qsCjs7VlVrJo8HnS4kJeDn5FGPWCwob6xw7wETOixz3C386 EtgKXEpxchmYWInikHx88EZfPIvAhbS3QaScrwEnLzEcSv68Y2YPV7oCAge0tIq2IOm2mf0iNze3 IOw/+6auntPItPYAAAAASUVORK5CYII=" x=".681" y=".641" transform="translate(-.68 -.64)"/>
            </svg> 
            <span class="fw-bold"> MiMiAs</span></a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" href="<?= $this->Url->build([
                                        'controller'=>'Mimias',
                                        'action'=>'index',
                                         $client->id,
                                        ]) ?>
            ">
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bookmark font-medium-3 me-50"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg>
            <span class="fw-bold"> Billing &amp; Plans</span>
          </a>
        </li>        
        <li class="nav-item">
          <a class="nav-link" href="<?= $this->Url->build([
                                        'controller'=>'Clients',
                                        'action'=>'gis',
                                         $client->id,
                                        ]) ?>">
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map"><polygon points="1 6 1 22 8 18 16 22 23 18 23 2 16 6 8 2 1 6"></polygon><line x1="8" y1="2" x2="8" y2="18"></line><line x1="16" y1="6" x2="16" y2="22"></line></svg><span class="fw-bold">Map &amp; Net</span>
          </a>
        </li>
      </ul>
      <!--/ User Pills -->

      <!-- Users table -->
      <div class="card">
        <h4 class="card-header">Active Users </h4>
        <div class="table-responsive">
          <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
            <table class="table table-stripes table-hover datatable-project dataTable no-footer dtr-column" id="DataTables_Table_0" role="grid" style="width: 953px;">
            <thead>
              <tr role="row">
                <th class="sorting_disabled control" rowspan="1" colspan="1" style="width: 0px; display: none;"></th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 300px;">User</th>
                <th class="text-nowrap sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">Roll</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 110px;">Logged</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 110px;">Last Login</th>
                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 110px;">Status</th>
              </tr>
            </thead>
            <tbody>
        <?php foreach ($users as $user): ?>

              <tr class="">                
                <td>
                  <div class="d-flex justify-content-left align-items-center">
                    <div class="avatar-wrapper">
                      <div class="avatar me-1">
                        <img src="/img/Users/avatar/<?= $user->get('avatar') ?
                                                        $user->get('avatar') : 'login.png'
                                                    ?> " alt="avatar" class="rounded-circle" width="32">
                      </div>
                    </div>
                    <div class="d-flex flex-column">
                      <a href="<?= $this->Url->build(
                                  [
                                    'controller'=>'users',
                                    'action'=>'view',
                                     $user->id,
                                  ]
                                ) 
                              ?>">
                        <span class="text-truncate fw-bolder"><?= $user->get('user') ?></span>
                      </a>
                    </div>
                  </div>
                </td>
                <td><span class="text-truncate fw-bolder"><?= $user->role->type ?></span></td>
                <td><span class="text-truncate fw-bolder"><?= $user->logged? 'Logged': 'Unlog' ?></span></td>
                <td><span class="text-truncate fw-bolder"><?= $user->last ?></span></td>
                <td><span class="text-truncate fw-bolder"><?= $user->act? 'Active': 'Inactive' ?></span></td>
              </tr>   
            <?php endforeach; ?> 
          
            </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /Users table -->

      <!-- Activity Timeline -->
      <div class="card">
        <h4 class="card-header">User Activity Timeline</h4>
        <div class="card-body pt-1">
          <ul class="timeline ms-50">
            
          </ul>
        </div>
      </div>
      <!-- /Activity Timeline -->

      <!-- Invoice table -->
      <div class="card">
        <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
          <div class="card-header pt-1 pb-25">
            <div class="head-label"><h4 class="card-title">Invoices</h4></div>
            <div class="dt-action-buttons text-end">
              <div class="dt-buttons d-inline-flex">
                <button class="dt-button buttons-collection btn btn-outline-secondary dropdown-toggle" tabindex="0" aria-controls="DataTables_Table_1" type="button" aria-haspopup="true">
                  <span><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-external-link font-small-4 me-50"><path d="M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6"></path><polyline points="15 3 21 3 21 9"></polyline><line x1="10" y1="14" x2="21" y2="3"></line></svg>Export</span>
                </button> 
              </div>
            </div>
          </div>
          <table class="invoice-table table text-nowrap dataTable no-footer dtr-column" id="DataTables_Table_1" role="grid">
          <thead>
            <tr role="row">
              <th class="control sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="display: none;" aria-label=": activate to sort column ascending"></th>
              <th class="sorting sorting_desc" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 46px;" aria-label="#ID: activate to sort column ascending" aria-sort="descending">#ID</th>
              <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 42px;" aria-label=": activate to sort column ascending">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trending-up"><polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline><polyline points="17 6 23 6 23 12"></polyline></svg>
              </th>
              <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 73px;" aria-label="TOTAL Paid: activate to sort column ascending">TOTAL Paid</th>
              <th class="text-truncate sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 130px;" aria-label="Issued Date: activate to sort column ascending">Issued Date</th>
              <th class="cell-fit sorting_disabled" rowspan="1" colspan="1" style="width: 80px;" aria-label="Actions">Actions</th>
            </tr>
          </thead>
        <tbody>
          <tr class="odd">
            <td class=" control" style="display: none;" tabindex="0"></td><td class="sorting_1">
              <a class="fw-bolder" href="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/html/ltr/horizontal-menu-template-dark/app-invoice-preview.html"> #5089</a>
            </td>
            <td><span data-bs-toggle="tooltip" data-bs-html="true" title="" data-bs-original-title="&lt;span&gt;Sent&lt;br&gt; &lt;span class=&quot;fw-bold&quot;&gt;Balance:&lt;/span&gt; 0&lt;br&gt; &lt;span class=&quot;fw-bold&quot;&gt;Due Date:&lt;/span&gt; 05/09/2019&lt;/span&gt;" aria-label="&lt;span&gt;Sent&lt;br&gt; &lt;span class=&quot;fw-bold&quot;&gt;Balance:&lt;/span&gt; 0&lt;br&gt; &lt;span class=&quot;fw-bold&quot;&gt;Due Date:&lt;/span&gt; 05/09/2019&lt;/span&gt;">
              <div class="avatar avatar-status bg-light-secondary">
                <span class="avatar-content">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-send avatar-icon"><line x1="22" y1="2" x2="11" y2="13"></line><polygon points="22 2 15 22 11 13 2 9 22 2"></polygon></svg>
                </span>
              </div>
            </span>
          </td>
          <td>$3077</td>
          <td>02 May 2019</td>
          <td>
            <div class="d-flex align-items-center col-actions">
            <a class="me-1" href="#" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Send Mail" aria-label="Send Mail">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-send font-medium-2 text-body"><line x1="22" y1="2" x2="11" y2="13"></line><polygon points="22 2 15 22 11 13 2 9 22 2"></polygon></svg>
            </a>
            <a class="me-1" href="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/html/ltr/horizontal-menu-template-dark/app-invoice-preview.html" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Preview Invoice" aria-label="Preview Invoice">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye font-medium-2 text-body"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle>
              </svg>
            </a>
            <a href="javascript:void(0)" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Download" aria-label="Download">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-download font-medium-2 text-body"><path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path><polyline points="7 10 12 15 17 10"></polyline><line x1="12" y1="15" x2="12" y2="3"></line>
              </svg>
            </a>
          </div>
        </td>
      </tr>
    </tbody></table></div>
      </div>
      <!-- /Invoice table -->
    </div>
    <!--/ User Content -->
  </div>
</section>

<!-- Edit User Modal -->
<div class="modal fade" id="editUser" tabindex="-1" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
    <div class="modal-content">
      <div class="modal-header bg-transparent">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body pb-5 px-sm-5 pt-50">
        <div class="text-center mb-2">
          <h1 class="mb-1">Edit Client Information</h1>
          <p>Updating client details will receive a privacy audit.</p>
        </div>
        <div >

          <?= $this->Form->create(
              $client,
              [
               'url'=>[
                        'controller'=>'Clients',
                        'action'=>'edit'
                      ],
                'id'=>'editUserForm',
                'class'=>'row gy-1 pt-75',
                'onsubmit'=>'return false',
                'novalidate'=>'novalidate',
                'enctype' => 'multipart/form-data',
              ]
            )
          ?>

          <div class="col-12">

            <?= $this->Form->control(
                'name',
                [                
                'label'  => ['text' => __('Company Name'),
                             'class' => 'form-label'],
                'class'  => 'form-control',
                'id'     => 'modalEditUserFirstName',
                'value'  => h($client->name),
                'placeholder' => 'ACME',
                'data-msg' => "Please enter company name"
                ]) ?>

          </div>

          <div class="col-12 col-md-6">          
            <label class="form-label"><?= __('Logo Image') ?></label>        
            <?=  $this->Form->file(
                'submittedfile',
                [
                  'class'  => 'form-control dt-file ',
                  'value' => __('Only png jpeg jpg'),
                ]
              ) 
            ?>          
          </div>          

          <div class="col-12 col-md-6">
            <label class="form-label"><?= __('Preview') ?></label>        
            <div class="d-flex align-items-center flex-column">
              <img class="img-fluid rounded " 
                src="<?= $client->get('file') ?
                                 $client->get('file') : '/img/Clients/logo/acme.jpeg'
                                ?>"
                alt="Client Logo" width="100" height="50">              
            </div>          
          </div>          

          <div class="col-12 col-md-6">
            
            <?= $this->Form->control(
              'companyType_id',
              [
                'opcions'=> $companyTypes,
                'label'  => ['text' => __('Company Type'),
                             'class' => 'form-label'],
                'class'  => 'form-select',
                'id'     => 'modalEditUserStatus', 
                'value'  => h($client->company_type_id),
                'placeholder' => '',
                'data-msg' => "Please enter company type"
              ]) ?>            
            
          </div>

          <div class="col-12 col-md-6">            

            <?= $this->Form->control(
                'vat',
                [                
                'label'  => [
                             'text' => __('Tax Id'),
                             'class' => 'form-label'
                           ],
                'type'   => 'number',           
                'class'  => 'form-control',
                'id'     => 'modalEditUserTaxId',
                'value'  => h($client->vat),
                'placeholder' => '20223334441',
                'data-msg' => "Please enter company vat id"
                ]) ?>

          </div>

          <div class="col-12 col-md-6">
            <label class="form-label" for="modalEditUserCountry">Country</label>
            <div class="position-relative">            
              <?php
                $opt= [
                'argentina' => 'Argentina',
                'brazil'    => 'Brazil',
                'bolivia'   => 'Bolivia',
                'chile'     => 'Chile',
                'mexico'    => 'Mexico',
                'paraguay'  => 'Paraguay',
                'peru'      => 'Peru',
                'uruguay'   => 'Uruguay'
                ]; 

                echo $this->Form->select(
                  'country',
                  $opt, 
                  [                                          
                    'class'       => 'form-select',
                    'id'          => 'modalEditUserCountry', 
                    'placeholder' => 'argentina'
                  ]
                );
              ?>
          </div>
          </div>      

          <div class="col-12 col-md-6">
              <?= $this->Form->control(
              'state',
              [
                'opcions'=> $states,
                'label'  => ['text' => __('Status'),
                             'class' => 'form-label'],
                'class'  => 'form-select',
                'id'     => 'modalEditUserStatus', 
                'value'  => h($client->state_id),
                'placeholder' => '',
                'data-msg' => "Please enter company state"
              ]) ?>              
          </div>                      
          
          <div class="col-12">

                <?= $this->Form->control(
                'web',
                [                
                'label'  => ['text' => 'Wed',
                             'class' => 'form-label'],
                'type'   => 'url',
                'class'  => 'form-control',
                'id'     => 'modalEditUserWeb',
                'value'  => h($client->web),
                'placeholder' => 'www.example.com',
                'data-msg' => "Please enter company web uri"
                ]) ?>
            
          </div>

          <div class="col-12 mb-2">

            <?= $this->Form->control(
                'address',
                [                
                'label'  => ['text' => __('State / City / Full Address'),
                             'class' => 'form-label'],
                'class'  => 'form-control',
                'id'     => 'modalEditUserAddress',
                'value'  => h($client->address),
                'placeholder' => 'Massachusetts, Springfield, Evergreen Terrace 742 ',
                'data-msg' => "Please enter company address"
                ]) ?>            

          </div>

          <hr class="solid mb-2">

          <div class="col-12 col-md-6">

            <?= $this->Form->control(
                'contact',
                [                
                'label'  => ['text' => __('Contact Name'),
                             'class' => 'form-label'],
                'class'  => 'form-control dt-full-name',
                'id'     => 'modalEditUserContact',
                'value'  => h($client->contact),
                'placeholder' => __('John Galt'),
                'data-msg' => "Please enter company contact full name"
                ]) ?>            
            
          </div>

          <div class="col-12 col-md-6">
            <?= $this->Form->control(
                'phone',
                [                
                'label'  => ['text' => __('Phone'),
                             'class' => 'form-label'],
                'type'   => 'tel',             
                'class'  => 'form-control phone-number-mask',
                'id'     => 'modalEditUserPhone',
                'value'  => h($client->phone),
                'placeholder' => '+54 (11) 933-4422',
                'data-msg' => "Please enter company contact phone"
                ]) ?>               
          </div>

          <div class="col-12 mb-2">

            <?= $this->Form->control(
                'email',
                [                
                'label'  => ['text' => __('Email'),
                             'class' => 'form-label'],
                'type'   => 'email',            
                'class'  => 'form-control dt-email',
                'id'     => 'modalEditUserEmail',
                'value'  => h($client->email),
                'placeholder' => __('john.galt@who.is'),
                'data-msg' => "Please enter company contact email"
                ]) ?>                  
          </div>                    

          <hr class="solid mb-2">          

          <div class="col-12">            
              <label class="form-label" for="basic-icon-default-note"><?= __('Comments') ?></label>
              <?= $this->Form->textarea(
                'note',
                [
                  'id'=>'basic-icon-default-contact',
                  'class'=>'form-control dt-note',
                  'value'  => h($client->note),
                  'placeholder'=>'Some things about'
                ]) ?>
          </div>
          
          <div class="col-12 text-center mt-2 pt-50">
            <?= $this->Form->button(
              __('Submit'),
              [            
                  'type'=> 'submit',      
                  'class'=>'btn btn-primary me-1 waves-effect waves-float waves-light',
              ]) ?>
            
            <button type="reset" class="btn btn-outline-secondary waves-effect" data-bs-dismiss="modal" aria-label="Close">
              Discard
            </button>
          </div>
           <?= $this->Form->end() ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!--/ Edit User Modal -->

<!-- upgrade your plan Modal -->
<div class="modal fade" id="upgradePlanModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-upgrade-plan">
    <div class="modal-content">
      <div class="modal-header bg-transparent">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body px-5 pb-2">
        <div class="text-center mb-2">
          <h1 class="mb-1">Upgrade Plan</h1>
          <p>Choose the best plan for user.</p>
        </div>        
        <?= $this->Form->create(
              $client,
              [
               'url'=>[
                        'controller'=>'Clients',
                        'action'=>'edit'
                      ],
                'id'=>'upgradePlanForm',
                'class'=>'row pt-50',
                'onsubmit'=>'return false',
                'novalidate'=>'novalidate',
              ]
            )
          ?>
          <div class="col-sm-8">
            <?= $this->Form->control(
              'plan',
              [
                'opcions'=> $plans,
                'label'  => ['text' => __('Choose Plan'),
                             'class' => 'form-label'],
                'class'  => 'form-select',
                'id'     => 'choosePlan',
                'value'  => h($client->state_id),
                'placeholder' => 'Choose Plan',
                'data-msg' => "Please enter plan"
              ]) ?>                    

          </div>

          <div class="col-sm-4 text-sm-end">
            <?= $this->Form->button(
                __('Upgrade'),
                [            
                    'type'=> 'submit',      
                    'class'=>'btn btn-primary mt-2 waves-effect waves-float waves-light',
                ]
              ) 
            ?>
            
          </div>
        <?= $this->Form->end() ?>        
      </div>
      <hr>
      <div class="modal-body px-5 pb-3">
        <h6><?= __('User current plan is '.h($client->plan->type).' plan') ?></h6>
        <div class="d-flex justify-content-between align-items-center flex-wrap">
          <div class="d-flex justify-content-center me-1 mb-1">
            <sup class="h5 pricing-currency pt-1 text-primary">UVA</sup>
            <h1 class="fw-bolder display-4 mb-0 text-primary me-25"><?= h($client->plan->worth) ?></h1>
            <sub class="pricing-duration font-small-4 mt-auto mb-2">/ per plan unit </sub>
          </div>
          <?= $this->Form->postLink(
              'Cancel Subscription',
              ['action' => 'edit', $client->id],
              [ 
                'confirm' => 'Are you sure?',
                'data'=>'Cancel Subscription',
                'class'=>'btn btn-outline-danger cancel-subscription mb-1 waves-effect',
              ],              
            )
          ?>          
        </div>
      </div>
    </div>
  </div>
</div>
<!--/ upgrade your plan Modal -->
      