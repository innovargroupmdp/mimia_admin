


<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Client> $clients
 */
?>

<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\User> $users
 * 
 * methods:
 * $this->block('nav');
   $this->block('menu');
   $this->block('path');
 */
 $this->element('home/headers'); 
?>

<?= $this->element('home/home_style_js_meta') ?>
<?= $this->element('Clients/list_style_js_meta') ?>

  <!-- entityz list start -->
<section id="dashboard-admin" class="app-user-list">
  <!-- entityz resume start-->
  
  <!-- list and filter start -->
  <div class="card">          

    <div class="card-body border-bottom">
      <h4 class="card-title"><?= __('Search &amp; Filter') ?></h4>
      <div class="row">
        <div class="col-md-4 user_role">
          <label class="form-label" for="ClientType"><?= __('Type') ?></label>
          <select id="UserRole" class="form-select text-capitalize mb-md-0 mb-2">
            <option value="" selected="selected"><?= __('Select type organization') ?></option>
            <option value="EDU" class="text-capitalize">EDU</option>
            <option value="COOPERATIVA" class="text-capitalize">COOPERATIVA</option>
            <option value="SA" class="text-capitalize">SA</option>
            <option value="SAS" class="text-capitalize">SAS</option>
            <option value="SRL" class="text-capitalize">SRL</option>
            <option value="Pers_FISICA" class="text-capitalize">Pers. FISICA</option>
            <option value="ONG" class="text-capitalize">ONG</option>
          </select>
        </div>
        <div class="col-md-4 user_plan">
          <label class="form-label" for="ClientPlan"><?= __('Plan') ?></label>
          <select id="ClientPlan" class="form-select text-capitalize mb-md-0 mb-2">
            <option value="" selected="selected"><?= __('Select Plan') ?></option>
            <option value="Basic" class="text-capitalize">Basic</option>
            <option value="Company" class="text-capitalize">Company</option>
            <option value="Enterprise" class="text-capitalize">Enterprise</option>
            <option value="Team" class="text-capitalize">Team</option>
          </select>
        </div>
        <div class="col-md-4 user_status">
          <label class="form-label" for="FilterTransaction"><?= __('Status') ?></label>
          <select id="FilterTransaction" class="form-select text-capitalize mb-md-0 mb-2xx">
            <option value="" selected="selected"><?= __('Select Status') ?></option>
            <option value="Pending" class="text-capitalize">Pending</option>
            <option value="Active" class="text-capitalize">Active</option>
            <option value="Inactive" class="text-capitalize">Inactive</option>
          </select>
        </div>
      </div>
    </div>

    <div class="card-datatable table-responsive pt-0">

      <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
        <div class="d-flex justify-content-between align-items-center header-actions mx-2 row mt-75">
          <div class="col-sm-12 col-lg-4 d-flex justify-content-center justify-content-lg-start">
            <div class="dataTables_length" id="DataTables_Table_0_length">
              <label>Show<select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="form-select">
                  <option value="10" selected="selected">10</option>
                  <option value="25">25</option>
                  <option value="50">50</option>
                  <option value="100">100</option>
                </select>entries</label>
            </div>
          </div>
          <div class="col-sm-12 col-lg-8 ps-xl-75 ps-0">
            <div class="dt-action-buttons d-flex align-items-center justify-content-center justify-content-lg-end flex-lg-nowrap flex-wrap">

              <div class="me-1">
                <div id="DataTables_Table_0_filter" class="dataTables_filter">
                  <label>Search:
                    <input type="search" class="form-control" placeholder="" aria-controls="DataTables_Table_0">
                  </label>
                </div>

              </div>
              <div class="dt-buttons d-inline-flex mt-50">

                <button class="dt-button buttons-collection btn btn-outline-secondary dropdown-toggle me-2" tabindex="0" aria-controls="DataTables_Table_0" type="button" aria-haspopup="true"><span><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-external-link font-small-4 me-50"><path d="M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6"></path><polyline points="15 3 21 3 21 9"></polyline><line x1="10" y1="14" x2="21" y2="3"></line></svg><?= __('Export') ?></span>
                </button>

                <button class="dt-button add-new btn btn-primary" tabindex="0" aria-controls="DataTables_Table_0" type="button" data-bs-toggle="modal" data-bs-target="#modals-slide-in"><span><?= __('Add New Client') ?></span>
                </button> 
                
              </div>
            </div>
          </div>
        </div>

        <table class="user-list-table table dataTable no-footer dtr-column" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="width: 1312px;">
          <thead class="table-light">
            <tr role="row">
              <th class="control sorting_disabled" rowspan="1" colspan="1" style="width: 0px; display: none;" aria-label=""></th>
              <th class="sorting sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 347px;" aria-label="Name: activate to sort column ascending" aria-sort="descending">Name</th>
              <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 150px;" aria-label="Role: activate to sort column ascending">Role</th>
              <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 110px;" aria-label="Plan: activate to sort column ascending">Plan</th>
              <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 213px;" aria-label="Billing: activate to sort column ascending">Billing</th>
              <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 96px;" aria-label="Status: activate to sort column ascending">Status</th>
              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 99px;" aria-label="Actions">Actions</th>
            </tr>
          </thead>

      <tbody>

        <tr class="odd">
          <td class=" control" style="display: none;" tabindex="0"></td>
          <td class="sorting_1">
            <div class="d-flex justify-content-left align-items-center">
              <div class="avatar-wrapper">
                <div class="avatar  me-1">
                  <img src="User%20List_archivos/2_002.png" alt="Avatar" width="32" height="32">
                </div>
              </div>
              <div class="d-flex flex-column"><a href="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/html/ltr/horizontal-menu-template-dark/app-user-view-account.html" class="user_name text-truncate text-body"><span class="fw-bolder">Zsazsa McCleverty</span></a><small class="emp_post text-muted">zmcclevertye@soundcloud.com</small>
              </div>
            </div>
          </td>
          <td>
              <span class="text-truncate align-middle"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-database font-medium-3 text-success me-50"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>Maintainer</span>
          </td>
          <td>Enterprise</td>
          <td>
            <span class="text-nowrap">Auto Debit</span>
          </td>
          <td>
            <span class="badge rounded-pill badge-light-success" text-capitalized="">Active</span>
          </td>
          <td>
            <div class="btn-group">
              <a class="btn btn-sm dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical font-small-4"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg></a>

              <div class="dropdown-menu dropdown-menu-end">
                
                <a href="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/html/ltr/horizontal-menu-template-dark/app-user-view-account.html" class="dropdown-item"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text font-small-4 me-50"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>Details</a>

                <a href="javascript:;" class="dropdown-item delete-record"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 font-small-4 me-50"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>Delete</a>

              </div>
            </div>
          </td>
        </tr> 

      </tbody>

        </table>

        <div class="d-flex justify-content-between mx-2 row mb-1">
          <div class="col-sm-12 col-md-6">
            <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 10 of 50 entries</div>
          </div>
          <div class="col-sm-12 col-md-6">
            <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
              <ul class="pagination">
                <li class="paginate_button page-item previous disabled" id="DataTables_Table_0_previous"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" class="page-link">&nbsp;</a>
                </li>
                <li class="paginate_button page-item active"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0" class="page-link">1</a>
                </li>
                <li class="paginate_button page-item "><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" class="page-link">2</a>
                </li>
                <li class="paginate_button page-item "><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="4" tabindex="0" class="page-link">4</a>
                </li>
                <li class="paginate_button page-item "><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="5" tabindex="0" class="page-link">5</a>
                </li>
                <li class="paginate_button page-item next" id="DataTables_Table_0_next"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="6" tabindex="0" class="page-link">&nbsp;</a>
                </li>
              </ul>
            </div>
          </div>
        </div>

      </div>

    </div>

    <!-- Modal to add new user starts-->
    <div class="modal modal-slide-in new-user-modal fade" id="modals-slide-in">
      <div class="modal-dialog">
        <form class="add-new-user modal-content pt-0" novalidate="novalidate">
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
          <div class="modal-header mb-1">
            <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
          </div>
          <div class="modal-body flex-grow-1">
            <div class="mb-1">
              <label class="form-label" for="basic-icon-default-fullname">Full Name</label>
              <input type="text" class="form-control dt-full-name" id="basic-icon-default-fullname" placeholder="John Doe" name="user-fullname">
            </div>
            <div class="mb-1">
              <label class="form-label" for="basic-icon-default-uname">Username</label>
              <input type="text" id="basic-icon-default-uname" class="form-control dt-uname" placeholder="Web Developer" name="user-name">
            </div>
            <div class="mb-1">
              <label class="form-label" for="basic-icon-default-email">Email</label>
              <input type="text" id="basic-icon-default-email" class="form-control dt-email" placeholder="john.doe@example.com" name="user-email">
            </div>
            <div class="mb-1">
              <label class="form-label" for="basic-icon-default-contact">Contact</label>
              <input type="text" id="basic-icon-default-contact" class="form-control dt-contact" placeholder="+1 (609) 933-44-22" name="user-contact">
            </div>
            <div class="mb-1">
              <label class="form-label" for="basic-icon-default-company">Company</label>
              <input type="text" id="basic-icon-default-company" class="form-control dt-contact" placeholder="PIXINVENT" name="user-company">
            </div>
            <div class="mb-1">
              <label class="form-label" for="country">Country</label>
              <div class="position-relative"><select id="country" class="select2 form-select select2-hidden-accessible" data-select2-id="country" tabindex="-1" aria-hidden="true">
                <option value="Australia" data-select2-id="2" selected="selected">USA</option>
                <option value="Bangladesh">Bangladesh</option>
                <option value="Belarus">Belarus</option>
                <option value="Brazil">Brazil</option>
                <option value="Canada">Canada</option>
                <option value="China">China</option>
                <option value="France">France</option>
                <option value="Germany">Germany</option>
                <option value="India">India</option>
                <option value="Indonesia">Indonesia</option>
                <option value="Israel">Israel</option>
                <option value="Italy">Italy</option>
                <option value="Japan">Japan</option>
                <option value="Korea">Korea, Republic of</option>
                <option value="Mexico">Mexico</option>
                <option value="Philippines">Philippines</option>
                <option value="Russia">Russian Federation</option>
                <option value="South Africa">South Africa</option>
                <option value="Thailand">Thailand</option>
                <option value="Turkey">Turkey</option>
                <option value="Ukraine">Ukraine</option>
                <option value="United Arab Emirates">United Arab Emirates</option>
                <option value="United Kingdom">United Kingdom</option>
                <option value="United States">United States</option>
              </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="1" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-country-container"><span class="select2-selection__rendered" id="select2-country-container" role="textbox" aria-readonly="true" title="USA">USA</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></div>
            </div>
            <div class="mb-1">
              <label class="form-label" for="user-role">User Role</label>
              <div class="position-relative"><select id="user-role" class="select2 form-select select2-hidden-accessible" data-select2-id="user-role" tabindex="-1" aria-hidden="true">
                <option value="subscriber" data-select2-id="4" selected="selected">Subscriber</option>
                <option value="editor">Editor</option>
                <option value="maintainer">Maintainer</option>
                <option value="author">Author</option>
                <option value="admin">Admin</option>
              </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="3" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-user-role-container"><span class="select2-selection__rendered" id="select2-user-role-container" role="textbox" aria-readonly="true" title="Subscriber">Subscriber</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></div>
            </div>
            <div class="mb-2">
              <label class="form-label" for="user-plan">Select Plan</label>
              <div class="position-relative"><select id="user-plan" class="select2 form-select select2-hidden-accessible" data-select2-id="user-plan" tabindex="-1" aria-hidden="true">
                <option value="basic" data-select2-id="6" selected="selected">Basic</option>
                <option value="enterprise">Enterprise</option>
                <option value="company">Company</option>
                <option value="team">Team</option>
              </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="5" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-user-plan-container"><span class="select2-selection__rendered" id="select2-user-plan-container" role="textbox" aria-readonly="true" title="Basic">Basic</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></div>
            </div>
            <button type="submit" class="btn btn-primary me-1 data-submit waves-effect waves-float waves-light"><?= __('Submit') ?></button>
            <button type="reset" class="btn btn-outline-secondary waves-effect" data-bs-dismiss="modal"><?= __('Cancel') ?></button>
          </div>
        </form>
      </div>
    </div>
    <!-- Modal to add new user Ends-->
  <!-- Modal to add new user Ends-->
  </div>
  <!-- list and filter end -->
</section>
  <!-- users list ends -->

<div class="clients index content">
  <?= $this->Html->link(__('Add new Client'), ['action' => 'add'], ['class' => 'button float-right']) ?>
  <h3><?= __('Clients') ?></h3>
  <div class="table-responsive">
    <table>
      <thead>
        <tr>
          <th><?= $this->Paginator->sort('client_tag') ?></th>
          <th><?= $this->Paginator->sort('name') ?></th>
          <th><?= $this->Paginator->sort('user') ?></th>
          <th><?= $this->Paginator->sort('password') ?></th>
          <th><?= $this->Paginator->sort('email') ?></th>
          <th><?= $this->Paginator->sort('phone') ?></th>
          <th><?= $this->Paginator->sort('contact') ?></th>
          <th><?= $this->Paginator->sort('cuit') ?></th>
          <th><?= $this->Paginator->sort('act') ?></th>
          <th><?= $this->Paginator->sort('enroll') ?></th>
          <th><?= $this->Paginator->sort('down') ?></th>
          <th><?= $this->Paginator->sort('comment') ?></th>
          <th class="actions"><?= __('Actions') ?></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($clients as $client): ?>
        <tr>
          <td><?= $this->Number->format($client->client_tag) ?></td>
          <td><?= h($client->name) ?></td>
          <td><?= h($client->type) ?></td>
          <td><?= h($client->user) ?></td>
          <td><?= h($client->password) ?></td>
          <td><?= h($client->email) ?></td>
          <td><?= h($client->phone) ?></td>
          <td><?= h($client->contact) ?></td>
          <td><?= h($client->vat_i) ?></td>          
          <td><?= h($client->country) ?></td>
          <td><?= h($client->plan) ?></td>
          <td><?= h($client->act) ?></td>
          <td><?= h($client->enroll) ?></td>
          <td><?= h($client->down) ?></td>
          <td><?= h($client->comment) ?></td>
          <td class="actions">
            <?= $this->Html->link(__('View'), ['action' => 'view', $client->id]) ?>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $client->id]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $client->id], ['confirm' => __('Are you sure you want to delete # {0}?', $client->id)]) ?>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
  
  <div class="paginator">
      <ul class="pagination">
          <?= $this->Paginator->first('<< ' . __('first')) ?>
          <?= $this->Paginator->prev('< ' . __('previous')) ?>
          <?= $this->Paginator->numbers() ?>
          <?= $this->Paginator->next(__('next') . ' >') ?>
          <?= $this->Paginator->last(__('last') . ' >>') ?>
      </ul>
      <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
  </div>
</div>
