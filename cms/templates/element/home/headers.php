<?php 
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\User> $users
 */
  $this->start('nav');
  echo $this->element('home/nav_bar');
  $this->end();

/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\User> $users
 */
  $this->start('menu');
  echo $this->element('home/main_menu');
  $this->end();

/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\User> $users
 */
  $this->start('path');
  echo $this->element('home/path');
  $this->end();

?>