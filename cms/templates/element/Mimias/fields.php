<?php
/**
 * @var \src\templates\element $this
 
 */
?>

<div class="column-responsive column-80">
  <div class="clients form content">
            <?= $this->Form->create($client) ?>
            <fieldset>
                <legend><?= __('Add Client') ?></legend>
                <?php
                    echo $this->Form->control('client_tag');
                    echo $this->Form->control('user');
                    echo $this->Form->control('key');
                    echo $this->Form->control('name');
                    echo $this->Form->control('cuit');
                    echo $this->Form->control('act');
                    echo $this->Form->control('enroll');
                    echo $this->Form->control('down');
                    echo $this->Form->control('comment');
                    echo $this->Form->control('gateways._ids', ['options' => $gateways]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
  </div>
</div>