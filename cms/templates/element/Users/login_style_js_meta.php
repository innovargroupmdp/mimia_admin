<?php
/**
* 
* Template Name: Login Admin Dashboard Template
* Author:  cbasll
* Website: www.innovar-groupmdq.com.ar
* Contact: cba.allende@gmail.com
* Follow:  www.twitter.com/CbasAll
* Like:    www.facebook.com/InnovarGroupmdq
* Purchase:info@innovar-groupmdq.com.ar 
*
* @var \src\templates\element $this
* @var
* 
*/
$pthSyl= 'login/';
?>


<?= $this->Html->meta(
    'icon',
    '/img/ico/igm_152_ico.png', 
    ['block' => true],
    ['type'=>'image/png']) ?>

<?= $this->Html->meta(
    'viewport',
    'width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui', 
    ['block' => true]) ?>

<?= $this->Html->meta(
    'description','MIMIA admin is super flexible, powerful, clean &amp; modern', 
    ['block' => true]) ?>

<?= $this->Html->meta(
    'keywords',
    'admin template, MIMIA admin, dashboard, flat admin, responsive admin, web app',
    ['block' => true]) ?>

<?= $this->Html->meta(
    'author',
    'IGMdP', 
    ['block' => true]) ?>



<?= $this->Html->css($pthSyl.'css2.css', ['block' => true]) ?>

<!-- BEGIN: Vendor CSS-->
<?= $this->Html->css($pthSyl.'vendors.min.css', ['block' => true]) ?>
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<?= $this->Html->css($pthSyl.'bootstrap.min.css',['block' => true]) ?> 
<?= $this->Html->css($pthSyl.'bootstrap-extended.min.css',['block' => true]) ?>
<?= $this->Html->css($pthSyl.'colors.min.css',['block' => true]) ?>
<?= $this->Html->css($pthSyl.'components.min.css',['block' => true]) ?>
<?= $this->Html->css($pthSyl.'dark-layout.min.css',['block' => true]) ?>
<?= $this->Html->css($pthSyl.'bordered-layout.min.css',['block' => true]) ?>
<?= $this->Html->css($pthSyl.'semi-dark-layout.min.css',['block' => true]) ?> 

<!-- BEGIN: Page CSS-->
<?= $this->Html->css( $pthSyl.'horizontal-menu.min.css',['block' => true]) ?>
<?= $this->Html->css( $pthSyl.'form-validation.css',['block' => true]) ?>
<?= $this->Html->css( $pthSyl.'authentication.css', ['block' => true]) ?>

<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<?= $this->Html->css($pthSyl.'style.css', ['block' => true]) ?>
<!-- END: Custom CSS-->




<!-- BEGIN: Vendor JS-->
<?= $this->Html->script(
    $pthSyl.'vendors.min.js', ['block' => true]) ?>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<?= $this->Html->script(
    $pthSyl.'jquery.sticky.js', ['block' => true]
) ?>
<?= $this->Html->script(
    $pthSyl.'jquery.validate.min.js', ['block' => true]) ?>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<?= $this->Html->script(
    $pthSyl.'app-menu.min.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'app.min.js', ['block' => true]) ?>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<?= $this->Html->script(
    $pthSyl.'auth-login.js', ['block' => true]) ?>
<!-- END: Page JS-->

<?php 
  $this->Html->scriptStart(['block' => true]);

  echo "  $(window).on('load',  function(){
      if (feather) {
        feather.replace({ width: 14, height: 14 });
      }
    })";

  $this->Html->scriptEnd();
?>
