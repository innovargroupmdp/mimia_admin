<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Gateway $gateway
 * @var \Cake\Collection\CollectionInterface|string[] $clients
 */
?>

<div class="column-responsive column-80">
    <div class="gateways form content">
        <?= $this->Form->create($gateway) ?>
        <fieldset>
            <legend><?= __('Add Gateway') ?></legend>
            <?php
                echo $this->Form->control('gw_tag');
                echo $this->Form->control('gw_mac');
                echo $this->Form->control('mac');
                echo $this->Form->control('key_svr0');
                echo $this->Form->control('key_svr');
                echo $this->Form->control('boot');
                echo $this->Form->control('boot_Init');
                echo $this->Form->control('release_version');
                echo $this->Form->control('act');
                echo $this->Form->control('on_service');
                echo $this->Form->control('off_service');
                echo $this->Form->control('description');
                echo $this->Form->control('comment');
                echo $this->Form->control('clients._ids', ['options' => $clients]);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Submit')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
