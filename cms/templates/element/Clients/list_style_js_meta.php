<?php
/**
* 
* Template Name: list Admin Dashboard Template
* Author:  cbasll
* Website: www.innovar-groupmdq.com.ar
* Contact: cba.allende@gmail.com
* Follow:  www.twitter.com/CbasAll
* Like:    www.facebook.com/InnovarGroupmdq
* Purchase:info@innovar-groupmdq.com.ar 
*
* @var \src\templates\element $this
* @var
* 
*/
$pthSyl= 'Clients/';
?>


<!-- BEGIN: Vendor CSS-->
<?= $this->Html->css($pthSyl.'select2.min.css', ['block' => true]) ?>
<?= $this->Html->css($pthSyl.'dataTables.bootstrap5.min.css', ['block' => true]) ?>
<?= $this->Html->css($pthSyl.'responsive.bootstrap5.min.css', ['block' => true]) ?>
<?= $this->Html->css($pthSyl.'buttons.bootstrap5.min.css', ['block' => true]) ?>
<?= $this->Html->css($pthSyl.'rowGroup.bootstrap5.min.css', ['block' => true]) ?>
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<!-- END: Theme CSS-->

<!-- BEGIN: Page CSS-->
<?= $this->Html->css( $pthSyl.'form-validation.css',['block' => true]) ?>
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<?= $this->Html->css($pthSyl.'style.css', ['block' => true]) ?>
<!-- END: Custom CSS-->


<!-- BEGIN: Vendor JS-->

<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<?= $this->Html->script(
    $pthSyl.'select2.full.min.js', ['block' => true]) ?>
<!-- $this->Html->script($pthSyl.'jquery.dataTables.min.js', ['block' => true])  -->
<?= $this->Html->script(
    $pthSyl.'dataTables.bootstrap5.min.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'dataTables.responsive.min.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'responsive.bootstrap5.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'datatables.buttons.min.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'jszip.min.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'pdfmake.min.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'vfs_fonts.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'buttons.html5.min.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'buttons.print.min.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'dataTables.rowGroup.min.js', ['block' => true]) ?>    
<?= $this->Html->script(
    $pthSyl.'jquery.validate.min.js', ['block' => true]) ?>    
<?= $this->Html->script(
    $pthSyl.'cleave.min.js', ['block' => true]) ?>            
<?= $this->Html->script(
    $pthSyl.'cleave-phone.us.js', ['block' => true]) ?>                
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->

<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<?= $this->Html->script(
    $pthSyl.'app-user-list.min.js', ['block' => true]) ?>
<!-- END: Page JS-->

