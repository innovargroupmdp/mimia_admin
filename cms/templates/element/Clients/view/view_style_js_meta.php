<?php
/**
* 
* Template Name: list Admin Dashboard Template
* Author:  cbasll
* Website: www.innovar-groupmdq.com.ar
* Contact: cba.allende@gmail.com
* Follow:  www.twitter.com/CbasAll
* Like:    www.facebook.com/InnovarGroupmdq
* Purchase:info@innovar-groupmdq.com.ar 
*
* @var \src\templates\element $this
* @var
* 
*/
$pthSyl= 'Clients/view/';
?>


<!-- BEGIN: Vendor CSS-->
<?= $this->Html->css($pthSyl.'animate.min.css', ['block' => true]) ?>
<?= $this->Html->css($pthSyl.'sweetalert2.min.css', ['block' => true]) ?>

<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<?= $this->Html->css($pthSyl.'ext-component-sweet-alerts.min.css', ['block' => true]) ?>
<!-- END: Theme CSS-->

<!-- BEGIN: Page CSS-->

<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<?= $this->Html->css($pthSyl.'style.css', ['block' => true]) ?>
<!-- END: Custom CSS-->




<!-- BEGIN: Vendor JS-->

<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<?= $this->Html->script(
    $pthSyl.'moment.min.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'polyfill.min.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'sweetalert2.all.min.js', ['block' => true]) ?>            
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->

<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<?= $this->Html->script(
    $pthSyl.'modal-edit-user.min.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'app-user-view-account.min.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'app-user-view.min.js', ['block' => true]) ?>
<?= $this->Html->script(
    $pthSyl.'page-account-settings-account.min.js', ['block' => true]) ?>
<!-- END: Page JS-->


