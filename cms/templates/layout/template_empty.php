<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$description = 'MIMIA: '.__('the IoT platform');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $Description ?>:
        <?= $this->fetch('title') ?>
    </title>
  </head>
  
  <body class="vertical-layout vertical-menu-modern navbar-floating footer-static" data-menu="vertical-menu-modern">

    <!-- BEGIN: Navbar -->
    <nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow">
      ...
    </nav>
    <!-- END: Navbar -->

    <!-- BEGIN: Main Menu -->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
      <div class="container">
      <?= $this->Flash->render() ?>
      <!-- insert here the view(parameter beforeRender) -->

      <!-- Render) -->
      <?= $this->fetch('content') ?>
      </div>
    </div>
    <!-- END: Main Menu -->

    <!-- BEGIN: Content -->
    <div class="app-content content">

        <!-- Content-wrapper -->
        <div class="content-wrapper">
          ...
        </div>

    </div>
    <!-- END: Content -->

    <!-- Begin: Footer -->
    <footer class="footer footer-static footer-light">
      ...
    </footer>
    <!-- END: Footer -->

  </body>
</html>