<?php
/**
 * 
* Template Name: Login Admin Dashboard Template
* Author:  cbasll
* Website: www.innovar-groupmdq.com.ar
* Contact: cba.allende@gmail.com
* Follow:  www.twitter.com/CbasAll
* Like:    www.facebook.com/InnovarGroupmdq
* Purchase:info@innovar-groupmdq.com.ar 
*
* @var \App\View\AppView $this
* 
*/

$description = 'MIMIA: '.__('the IoT platform');
$pthMd = 'img/';
$pthSyl= 'login/'; 
?>

<!DOCTYPE html>
<!--
Template Name: Home Admin Dashboard Template
Author:  cbasll
Website: www.innovar-groupmdq.com.ar
Contact: cba.allende@gmail.com
Follow:  www.twitter.com/CbasAll
Like:    www.facebook.com/InnovarGroupmdq
Purchase:info@innovar-groupmdq.com.ar 

License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html class="dark-layout loaded" data-layout="dark-layout" data-textdirection="ltr" style="--vh: 6.5600000000000005px;" lang="en"><!-- BEGIN: Head-->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      
    <title>
      <?= $this->fetch('title') ?>
      <?= $description ?>:
    </title>
    <!-- Asure the styles if in block is not present-->      
    <!-- bring the code in blocks -->     
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="pace-done horizontal-layout navbar-floating footer-static menu-expanded horizontal-menu" data-open="hover" data-menu="horizontal-menu" data-col="">
    <div class="pace pace-inactive">
      <div class="pace-progress" style="transform: translate3d(100%, 0px, 0px);" data-progress-text="100%" data-progress="99">
        <div class="pace-progress-inner"></div>
      </div>
      <div class="pace-activity"></div>
    </div>

  <!-- BEGIN: Header-->

    <!-- BEGIN: Navbar -->
    <nav class="header-navbar navbar-expand-lg navbar navbar-fixed align-items-center navbar-shadow navbar-brand-center" data-nav="brand-center">

    <!-- BEGIN: Leyend -->
      <?= $this->fetch('nav') ?>
      
    <!-- BEGIN: Items -->
      <?= $this->fetch('items') ?>
      
    </nav>
    <!-- END: Navbar -->

  <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="horizontal-menu-wrapper">

      <?= $this->fetch('menu') ?>

    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>

      <div class="content-wrapper container-xxl p-0">
        <!-- BEGIN: content-HEADER-->
        <div class="content-header row">
          <div class="content-header-left col-md-9 col-12 mb-2">
            <!-- BEGIN: path-->
            <?= $this->fetch('path') ?>
            <!-- END: path-->
          </div>
          <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
            <div class="mb-1 breadcrumb-right">    </div></div>
        </div>
<!------------- BODY App DashBoard Starts ---------------------->
        <div class="content-body">

          <!-- BEGIN: content-ALERTS-->
          <div class="row">
            <div class="col-12">
            <div class="alert alert-primary" role="alert">
              <div class="alert-body">
                <strong> <?= $this->Flash->render() ?></strong>
              </div>
            </div>
            </div>
          </div>
          <!-- END: content-ALERTS-->

<!------------- Dashboard App DashBoard Starts ---------------------->
          <!-- <section id="dashboard-admin"> -->            
          <!-- insert here the view(parameter beforeRender) -->
          <!-- Render) -->
            <?= $this->fetch('content') ?>

          <!-- </section> -->                
          <!-- Modals -->                 
<!------------- Dashboard App DashBoard Ends ------------------------>

        </div><!-- END: content-BODY-->
<!------------- BODY App DashBoard Ends ---------------------->    
      </div><!-- END: content-wrapper-->
    </div>
    <!-- END: Content-->

<!-- BEGIN: Customizer-->
<?= $this->fetch('extras') ?>
<!-- End: Customizer-->

<!-- BEGIN: Footer-->
<?= $this->fetch('footer') ?>
<!-- END: Footer-->

<?= $this->fetch('script') ?>
    
</body><!-- END: Body--></html>