<?php
/**
 * 
* Template Name: Login Admin Dashboard Template
* Author:  cbasll
* Website: www.innovar-groupmdq.com.ar
* Contact: cba.allende@gmail.com
* Follow:  www.twitter.com/CbasAll
* Like:    www.facebook.com/InnovarGroupmdq
* Purchase:info@innovar-groupmdq.com.ar 
*
* @var \App\View\AppView $this
* 
*/

$description = 'MIMIA: '.__('the IoT platform');
$pthMd = 'img/';
$pthSyl= 'login_files/'; 
?>
<!DOCTYPE html>
<!--
Template Name: Login Admin Dashboard Template
Author:  cbasll
Website: www.innovar-groupmdq.com.ar
Contact: cba.allende@gmail.com
Follow:  www.twitter.com/CbasAll
Like:    www.facebook.com/InnovarGroupmdq
Purchase:info@innovar-groupmdq.com.ar 

License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html class="dark-layout loaded" data-layout="dark-layout" data-textdirection="ltr" style="--vh: 6.5600000000000005px;" lang="en"><!-- BEGIN: Head-->
<head>

  <?= $this->Html->charset('utf-8') ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">    

  <title>
    <?= $this->fetch('title') ?>
    <?= $description ?>:
  </title>
  <!-- Asure the styles if in block is not present-->      
  <!-- bring the code in blocks -->     
  <?= $this->fetch('meta') ?>
  <?= $this->fetch('css') ?>
  <?= $this->fetch('script') ?>

</head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="pace-done horizontal-layout blank-page navbar-floating footer-static menu-expanded horizontal-menu" data-open="hover" data-menu="horizontal-menu" data-col="blank-page">
    <div class="pace pace-inactive">
      <div class="pace-progress" style="transform: translate3d(100%, 0px, 0px);" data-progress-text="100%" data-progress="99">
      <div class="pace-progress-inner"></div>
      </div>
      <div class="pace-activity"></div>
    </div>
    
    <!-- BEGIN: Content-->
    <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>

      <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body">
          <div class="auth-wrapper auth-cover">
            <div class="auth-inner row m-0">
              
              <!-- Brand logo-->                
              <a class="brand-logo" href="https://innovar-groupmdq.com.ar">
                  <img src="img/login/igm_brand_light_logo.svg" alt="IGM" height="40">
                <!-- <h2 class="brand-text text-primary ms-1">MiMiA</h2>-->
              </a>                
              <!-- /Brand logo-->
              
              <!-- Left Text-->
              <div id="section1" class="d-none d-lg-flex col-lg-8 align-items-center p-5" >                
                <div class="w-100 d-lg-flex align-items-center justify-content-center px-5"><img class="img-fluid" src="img/login/mimia_logo.svg" alt="Login V2">
                </div>
              </div>
              <!-- /Left Text-->

              <!-- Login-->

                <!-- $this->Flash->render() ?> -->
                <!-- insert here the view(parameter beforeRender) -->
    
                <!-- Render) -->
                <?= $this->fetch('content') ?>

              <!-- /Login-->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END: Content-->  
  
</body><!-- END: Body--></html>