<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Model> $models
 */
?>
<div class="models index content">
    <?= $this->Html->link(__('New Model'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Models') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('device_type_id') ?></th>
                    <th><?= $this->Paginator->sort('release_version') ?></th>
                    <th><?= $this->Paginator->sort('description') ?></th>
                    <th><?= $this->Paginator->sort('producction') ?></th>
                    <th><?= $this->Paginator->sort('picture') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($models as $model): ?>
                <tr>
                    <td><?= $this->Number->format($model->id) ?></td>
                    <td><?= $model->has('device_type') ? $this->Html->link($model->device_type->id, ['controller' => 'DeviceTypes', 'action' => 'view', $model->device_type->id]) : '' ?></td>
                    <td><?= h($model->release_version) ?></td>
                    <td><?= h($model->description) ?></td>
                    <td><?= h($model->producction) ?></td>
                    <td><?= h($model->picture) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $model->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $model->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $model->id], ['confirm' => __('Are you sure you want to delete # {0}?', $model->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
