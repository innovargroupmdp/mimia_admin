<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Model $model
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Model'), ['action' => 'edit', $model->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Model'), ['action' => 'delete', $model->id], ['confirm' => __('Are you sure you want to delete # {0}?', $model->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Models'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Model'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="models view content">
            <h3><?= h($model->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Device Type') ?></th>
                    <td><?= $model->has('device_type') ? $this->Html->link($model->device_type->id, ['controller' => 'DeviceTypes', 'action' => 'view', $model->device_type->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Release Version') ?></th>
                    <td><?= h($model->release_version) ?></td>
                </tr>
                <tr>
                    <th><?= __('Description') ?></th>
                    <td><?= h($model->description) ?></td>
                </tr>
                <tr>
                    <th><?= __('Picture') ?></th>
                    <td><?= h($model->picture) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($model->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Producction') ?></th>
                    <td><?= $model->producction ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Gateways') ?></h4>
                <?php if (!empty($model->gateways)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Gw Tag') ?></th>
                            <th><?= __('Gw Mac') ?></th>
                            <th><?= __('Mac') ?></th>
                            <th><?= __('Key Svr0') ?></th>
                            <th><?= __('Key Svr') ?></th>
                            <th><?= __('Boot') ?></th>
                            <th><?= __('Boot Init') ?></th>
                            <th><?= __('Release Version') ?></th>
                            <th><?= __('Model Id') ?></th>
                            <th><?= __('Act') ?></th>
                            <th><?= __('On Service') ?></th>
                            <th><?= __('Off Service') ?></th>
                            <th><?= __('Client Id') ?></th>
                            <th><?= __('Comment') ?></th>
                            <th><?= __('Gis') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($model->gateways as $gateways) : ?>
                        <tr>
                            <td><?= h($gateways->id) ?></td>
                            <td><?= h($gateways->gw_tag) ?></td>
                            <td><?= h($gateways->gw_mac) ?></td>
                            <td><?= h($gateways->mac) ?></td>
                            <td><?= h($gateways->key_svr0) ?></td>
                            <td><?= h($gateways->key_svr) ?></td>
                            <td><?= h($gateways->boot) ?></td>
                            <td><?= h($gateways->boot_Init) ?></td>
                            <td><?= h($gateways->release_version) ?></td>
                            <td><?= h($gateways->model_id) ?></td>
                            <td><?= h($gateways->act) ?></td>
                            <td><?= h($gateways->on_service) ?></td>
                            <td><?= h($gateways->off_service) ?></td>
                            <td><?= h($gateways->client_id) ?></td>
                            <td><?= h($gateways->comment) ?></td>
                            <td><?= h($gateways->gis) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Gateways', 'action' => 'view', $gateways->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Gateways', 'action' => 'edit', $gateways->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Gateways', 'action' => 'delete', $gateways->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gateways->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
