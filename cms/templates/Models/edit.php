<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Model $model
 * @var string[]|\Cake\Collection\CollectionInterface $deviceTypes
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $model->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $model->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Models'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="models form content">
            <?= $this->Form->create($model) ?>
            <fieldset>
                <legend><?= __('Edit Model') ?></legend>
                <?php
                    echo $this->Form->control('device_type_id', ['options' => $deviceTypes]);
                    echo $this->Form->control('release_version');
                    echo $this->Form->control('description');
                    echo $this->Form->control('producction');
                    echo $this->Form->control('picture');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
