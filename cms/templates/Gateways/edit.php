<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Gateway $gateway
 * @var string[]|\Cake\Collection\CollectionInterface $clients
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $gateway->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $gateway->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Gateways'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="gateways form content">
            <?= $this->Form->create($gateway) ?>
            <fieldset>
                <legend><?= __('Edit Gateway') ?></legend>
                <?php
                    echo $this->Form->control('gw_tag');
                    echo $this->Form->control('gw_mac');
                    echo $this->Form->control('mac');
                    echo $this->Form->control('key_svr0');
                    echo $this->Form->control('key_svr');
                    echo $this->Form->control('boot');
                    echo $this->Form->control('boot_Init');
                    echo $this->Form->control('release_version');
                    echo $this->Form->control('model_id');
                    echo $this->Form->control('act');
                    echo $this->Form->control('on_service', ['empty' => true]);
                    echo $this->Form->control('off_service', ['empty' => true]);
                    echo $this->Form->control('client_id', ['options' => $clients]);
                    echo $this->Form->control('comment');
                    echo $this->Form->control('gis');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
