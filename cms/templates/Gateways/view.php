<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Gateway $gateway
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Gateway'), ['action' => 'edit', $gateway->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Gateway'), ['action' => 'delete', $gateway->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gateway->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Gateways'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Gateway'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="gateways view content">
            <h3><?= h($gateway->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Mac') ?></th>
                    <td><?= h($gateway->mac) ?></td>
                </tr>
                <tr>
                    <th><?= __('Key Svr0') ?></th>
                    <td><?= h($gateway->key_svr0) ?></td>
                </tr>
                <tr>
                    <th><?= __('Key Svr') ?></th>
                    <td><?= h($gateway->key_svr) ?></td>
                </tr>
                <tr>
                    <th><?= __('Release Version') ?></th>
                    <td><?= h($gateway->release_version) ?></td>
                </tr>
                <tr>
                    <th><?= __('Client') ?></th>
                    <td><?= $gateway->has('client') ? $this->Html->link($gateway->client->name, ['controller' => 'Clients', 'action' => 'view', $gateway->client->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Comment') ?></th>
                    <td><?= h($gateway->comment) ?></td>
                </tr>
                <tr>
                    <th><?= __('Gis') ?></th>
                    <td><?= h($gateway->gis) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($gateway->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Gw Tag') ?></th>
                    <td><?= $this->Number->format($gateway->gw_tag) ?></td>
                </tr>
                <tr>
                    <th><?= __('Gw Mac') ?></th>
                    <td><?= $this->Number->format($gateway->gw_mac) ?></td>
                </tr>
                <tr>
                    <th><?= __('Model Id') ?></th>
                    <td><?= $this->Number->format($gateway->model_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('On Service') ?></th>
                    <td><?= h($gateway->on_service) ?></td>
                </tr>
                <tr>
                    <th><?= __('Off Service') ?></th>
                    <td><?= h($gateway->off_service) ?></td>
                </tr>
                <tr>
                    <th><?= __('Boot') ?></th>
                    <td><?= $gateway->boot ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th><?= __('Boot Init') ?></th>
                    <td><?= $gateway->boot_Init ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th><?= __('Act') ?></th>
                    <td><?= $gateway->act ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Measures') ?></h4>
                <?php if (!empty($gateway->measures)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Slot') ?></th>
                            <th><?= __('Trie N') ?></th>
                            <th><?= __('Tx Power') ?></th>
                            <th><?= __('Noise Lv') ?></th>
                            <th><?= __('Rx Signal') ?></th>
                            <th><?= __('Battery Lv') ?></th>
                            <th><?= __('Current Tx Date') ?></th>
                            <th><?= __('Current Measure') ?></th>
                            <th><?= __('Current Measure Date') ?></th>
                            <th><?= __('Measure Dif File') ?></th>
                            <th><?= __('Mimia Id') ?></th>
                            <th><?= __('Gateway Id') ?></th>
                            <th><?= __('Comment') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($gateway->measures as $measures) : ?>
                        <tr>
                            <td><?= h($measures->id) ?></td>
                            <td><?= h($measures->slot) ?></td>
                            <td><?= h($measures->trie_n) ?></td>
                            <td><?= h($measures->tx_power) ?></td>
                            <td><?= h($measures->noise_lv) ?></td>
                            <td><?= h($measures->rx_signal) ?></td>
                            <td><?= h($measures->battery_lv) ?></td>
                            <td><?= h($measures->current_tx_date) ?></td>
                            <td><?= h($measures->current_measure) ?></td>
                            <td><?= h($measures->current_measure_date) ?></td>
                            <td><?= h($measures->measure_dif_file) ?></td>
                            <td><?= h($measures->mimia_id) ?></td>
                            <td><?= h($measures->gateway_id) ?></td>
                            <td><?= h($measures->comment) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Measures', 'action' => 'view', $measures->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Measures', 'action' => 'edit', $measures->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Measures', 'action' => 'delete', $measures->id], ['confirm' => __('Are you sure you want to delete # {0}?', $measures->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
