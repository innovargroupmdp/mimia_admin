<div class="gateways index content">
    <?= $this->Html->link(__('New Gateway'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Gateways') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('gw_tag') ?></th>
                    <th><?= $this->Paginator->sort('gw_mac') ?></th>
                    <th><?= $this->Paginator->sort('mac') ?></th>
                    <th><?= $this->Paginator->sort('key_svr0') ?></th>
                    <th><?= $this->Paginator->sort('key_svr') ?></th>
                    <th><?= $this->Paginator->sort('boot') ?></th>
                    <th><?= $this->Paginator->sort('boot_Init') ?></th>
                    <th><?= $this->Paginator->sort('release_version') ?></th>
                    <th><?= $this->Paginator->sort('act') ?></th>
                    <th><?= $this->Paginator->sort('on_service') ?></th>
                    <th><?= $this->Paginator->sort('off_service') ?></th>
                    <th><?= $this->Paginator->sort('client_id') ?></th>
                    <th><?= $this->Paginator->sort('description') ?></th>
                    <th><?= $this->Paginator->sort('comment') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($gateways as $gateway): ?>
                <tr>
                    <td><?= $this->Number->format($gateway->id) ?></td>
                    <td><?= $this->Number->format($gateway->gw_tag) ?></td>
                    <td><?= $this->Number->format($gateway->gw_mac) ?></td>
                    <td><?= h($gateway->mac) ?></td>
                    <td><?= h($gateway->key_svr0) ?></td>
                    <td><?= h($gateway->key_svr) ?></td>
                    <td><?= h($gateway->boot) ?></td>
                    <td><?= h($gateway->boot_Init) ?></td>
                    <td><?= h($gateway->release_version) ?></td>
                    <td><?= h($gateway->act) ?></td>
                    <td><?= h($gateway->on_service) ?></td>
                    <td><?= h($gateway->off_service) ?></td>
                    <td><?= $this->Number->format($gateway->client_id) ?></td>
                    <td><?= h($gateway->description) ?></td>
                    <td><?= h($gateway->comment) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $gateway->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $gateway->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $gateway->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gateway->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>