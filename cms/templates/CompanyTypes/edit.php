<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CompanyType $companyType
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $companyType->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $companyType->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Company Types'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="companyTypes form content">
            <?= $this->Form->create($companyType) ?>
            <fieldset>
                <legend><?= __('Edit Company Type') ?></legend>
                <?php
                    echo $this->Form->control('type');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
