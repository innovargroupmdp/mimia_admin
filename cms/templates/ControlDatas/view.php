<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ControlData $controlData
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Control Data'), ['action' => 'edit', $controlData->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Control Data'), ['action' => 'delete', $controlData->id], ['confirm' => __('Are you sure you want to delete # {0}?', $controlData->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Control Datas'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Control Data'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="controlDatas view content">
            <h3><?= h($controlData->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Data Ctrl File') ?></th>
                    <td><?= h($controlData->data_ctrl_file) ?></td>
                </tr>
                <tr>
                    <th><?= __('Device') ?></th>
                    <td><?= $controlData->has('device') ? $this->Html->link($controlData->device->id, ['controller' => 'Devices', 'action' => 'view', $controlData->device->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($controlData->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Subtype') ?></th>
                    <td><?= $this->Number->format($controlData->subtype) ?></td>
                </tr>
                <tr>
                    <th><?= __('Order Date') ?></th>
                    <td><?= h($controlData->order_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Rx Tx') ?></th>
                    <td><?= $controlData->rx_tx ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
