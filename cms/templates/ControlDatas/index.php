<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\ControlData> $controlDatas
 */
?>
<div class="controlDatas index content">
    <?= $this->Html->link(__('New Control Data'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Control Datas') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('subtype') ?></th>
                    <th><?= $this->Paginator->sort('data_ctrl_file') ?></th>
                    <th><?= $this->Paginator->sort('rx_tx') ?></th>
                    <th><?= $this->Paginator->sort('order_date') ?></th>
                    <th><?= $this->Paginator->sort('device_id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($controlDatas as $controlData): ?>
                <tr>
                    <td><?= $this->Number->format($controlData->id) ?></td>
                    <td><?= $this->Number->format($controlData->subtype) ?></td>
                    <td><?= h($controlData->data_ctrl_file) ?></td>
                    <td><?= h($controlData->rx_tx) ?></td>
                    <td><?= h($controlData->order_date) ?></td>
                    <td><?= $controlData->has('device') ? $this->Html->link($controlData->device->id, ['controller' => 'Devices', 'action' => 'view', $controlData->device->id]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $controlData->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $controlData->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $controlData->id], ['confirm' => __('Are you sure you want to delete # {0}?', $controlData->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
