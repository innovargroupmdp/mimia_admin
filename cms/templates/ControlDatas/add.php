<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ControlData $controlData
 * @var \Cake\Collection\CollectionInterface|string[] $devices
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Control Datas'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="controlDatas form content">
            <?= $this->Form->create($controlData) ?>
            <fieldset>
                <legend><?= __('Add Control Data') ?></legend>
                <?php
                    echo $this->Form->control('subtype');
                    echo $this->Form->control('data_ctrl_file');
                    echo $this->Form->control('rx_tx');
                    echo $this->Form->control('order_date');
                    echo $this->Form->control('device_id', ['options' => $devices]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
