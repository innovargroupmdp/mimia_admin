<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeviceType $deviceType
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $deviceType->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $deviceType->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Device Types'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="deviceTypes form content">
            <?= $this->Form->create($deviceType) ?>
            <fieldset>
                <legend><?= __('Edit Device Type') ?></legend>
                <?php
                    echo $this->Form->control('type');
                    echo $this->Form->control('description');
                    echo $this->Form->control('symbol');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
