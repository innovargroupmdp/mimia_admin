<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DeviceType $deviceType
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Device Type'), ['action' => 'edit', $deviceType->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Device Type'), ['action' => 'delete', $deviceType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $deviceType->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Device Types'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Device Type'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="deviceTypes view content">
            <h3><?= h($deviceType->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Type') ?></th>
                    <td><?= h($deviceType->type) ?></td>
                </tr>
                <tr>
                    <th><?= __('Description') ?></th>
                    <td><?= h($deviceType->description) ?></td>
                </tr>
                <tr>
                    <th><?= __('Symbol') ?></th>
                    <td><?= h($deviceType->symbol) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($deviceType->id) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Models') ?></h4>
                <?php if (!empty($deviceType->models)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Device Type Id') ?></th>
                            <th><?= __('Release Version') ?></th>
                            <th><?= __('Description') ?></th>
                            <th><?= __('Producction') ?></th>
                            <th><?= __('Picture') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($deviceType->models as $models) : ?>
                        <tr>
                            <td><?= h($models->id) ?></td>
                            <td><?= h($models->device_type_id) ?></td>
                            <td><?= h($models->release_version) ?></td>
                            <td><?= h($models->description) ?></td>
                            <td><?= h($models->producction) ?></td>
                            <td><?= h($models->picture) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Models', 'action' => 'view', $models->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Models', 'action' => 'edit', $models->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Models', 'action' => 'delete', $models->id], ['confirm' => __('Are you sure you want to delete # {0}?', $models->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
