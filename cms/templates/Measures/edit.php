<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Measure $measure
 * @var string[]|\Cake\Collection\CollectionInterface $mimias
 * @var string[]|\Cake\Collection\CollectionInterface $gateways
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $measure->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $measure->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Measures'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="measures form content">
            <?= $this->Form->create($measure) ?>
            <fieldset>
                <legend><?= __('Edit Measure') ?></legend>
                <?php
                    echo $this->Form->control('slot');
                    echo $this->Form->control('trie_n');
                    echo $this->Form->control('tx_power');
                    echo $this->Form->control('noise_lv');
                    echo $this->Form->control('rx_signal');
                    echo $this->Form->control('battery_lv');
                    echo $this->Form->control('current_tx_date');
                    echo $this->Form->control('current_measure');
                    echo $this->Form->control('current_measure_date');
                    echo $this->Form->control('measure_dif_file');
                    echo $this->Form->control('mimia_id', ['options' => $mimias]);
                    echo $this->Form->control('gateway_id', ['options' => $gateways]);
                    echo $this->Form->control('comment');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
