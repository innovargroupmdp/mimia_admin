<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Measure $measure
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Measure'), ['action' => 'edit', $measure->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Measure'), ['action' => 'delete', $measure->id], ['confirm' => __('Are you sure you want to delete # {0}?', $measure->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Measures'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Measure'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="measures view content">
            <h3><?= h($measure->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Measure Dif File') ?></th>
                    <td><?= h($measure->measure_dif_file) ?></td>
                </tr>
                <tr>
                    <th><?= __('Mimia') ?></th>
                    <td><?= $measure->has('mimia') ? $this->Html->link($measure->mimia->id, ['controller' => 'Mimias', 'action' => 'view', $measure->mimia->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Gateway') ?></th>
                    <td><?= $measure->has('gateway') ? $this->Html->link($measure->gateway->id, ['controller' => 'Gateways', 'action' => 'view', $measure->gateway->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Comment') ?></th>
                    <td><?= h($measure->comment) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($measure->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Slot') ?></th>
                    <td><?= $this->Number->format($measure->slot) ?></td>
                </tr>
                <tr>
                    <th><?= __('Trie N') ?></th>
                    <td><?= $this->Number->format($measure->trie_n) ?></td>
                </tr>
                <tr>
                    <th><?= __('Tx Power') ?></th>
                    <td><?= $this->Number->format($measure->tx_power) ?></td>
                </tr>
                <tr>
                    <th><?= __('Noise Lv') ?></th>
                    <td><?= $this->Number->format($measure->noise_lv) ?></td>
                </tr>
                <tr>
                    <th><?= __('Rx Signal') ?></th>
                    <td><?= $this->Number->format($measure->rx_signal) ?></td>
                </tr>
                <tr>
                    <th><?= __('Battery Lv') ?></th>
                    <td><?= $this->Number->format($measure->battery_lv) ?></td>
                </tr>
                <tr>
                    <th><?= __('Current Measure') ?></th>
                    <td><?= $this->Number->format($measure->current_measure) ?></td>
                </tr>
                <tr>
                    <th><?= __('Current Tx Date') ?></th>
                    <td><?= h($measure->current_tx_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Current Measure Date') ?></th>
                    <td><?= h($measure->current_measure_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
