<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Measure> $measures
 */
?>
<div class="measures index content">
    <?= $this->Html->link(__('New Measure'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Measures') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('slot') ?></th>
                    <th><?= $this->Paginator->sort('trie_n') ?></th>
                    <th><?= $this->Paginator->sort('tx_power') ?></th>
                    <th><?= $this->Paginator->sort('noise_lv') ?></th>
                    <th><?= $this->Paginator->sort('rx_signal') ?></th>
                    <th><?= $this->Paginator->sort('battery_lv') ?></th>
                    <th><?= $this->Paginator->sort('current_tx_date') ?></th>
                    <th><?= $this->Paginator->sort('current_measure') ?></th>
                    <th><?= $this->Paginator->sort('current_measure_date') ?></th>
                    <th><?= $this->Paginator->sort('measure_dif_file') ?></th>
                    <th><?= $this->Paginator->sort('mimia_id') ?></th>
                    <th><?= $this->Paginator->sort('gateway_id') ?></th>
                    <th><?= $this->Paginator->sort('comment') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($measures as $measure): ?>
                <tr>
                    <td><?= $this->Number->format($measure->id) ?></td>
                    <td><?= $this->Number->format($measure->slot) ?></td>
                    <td><?= $this->Number->format($measure->trie_n) ?></td>
                    <td><?= $this->Number->format($measure->tx_power) ?></td>
                    <td><?= $this->Number->format($measure->noise_lv) ?></td>
                    <td><?= $this->Number->format($measure->rx_signal) ?></td>
                    <td><?= $this->Number->format($measure->battery_lv) ?></td>
                    <td><?= h($measure->current_tx_date) ?></td>
                    <td><?= $this->Number->format($measure->current_measure) ?></td>
                    <td><?= h($measure->current_measure_date) ?></td>
                    <td><?= h($measure->measure_dif_file) ?></td>
                    <td><?= $measure->has('mimia') ? $this->Html->link($measure->mimia->id, ['controller' => 'Mimias', 'action' => 'view', $measure->mimia->id]) : '' ?></td>
                    <td><?= $measure->has('gateway') ? $this->Html->link($measure->gateway->id, ['controller' => 'Gateways', 'action' => 'view', $measure->gateway->id]) : '' ?></td>
                    <td><?= h($measure->comment) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $measure->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $measure->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $measure->id], ['confirm' => __('Are you sure you want to delete # {0}?', $measure->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
