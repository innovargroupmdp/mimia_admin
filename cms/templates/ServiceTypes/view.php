<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ServiceType $serviceType
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Service Type'), ['action' => 'edit', $serviceType->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Service Type'), ['action' => 'delete', $serviceType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $serviceType->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Service Types'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Service Type'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="serviceTypes view content">
            <h3><?= h($serviceType->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Type') ?></th>
                    <td><?= h($serviceType->type) ?></td>
                </tr>
                <tr>
                    <th><?= __('Unit') ?></th>
                    <td><?= h($serviceType->unit) ?></td>
                </tr>
                <tr>
                    <th><?= __('Comment') ?></th>
                    <td><?= h($serviceType->comment) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($serviceType->id) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Mimias') ?></h4>
                <?php if (!empty($serviceType->mimias)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Mimia Tag') ?></th>
                            <th><?= __('Mm Mac') ?></th>
                            <th><?= __('Mac') ?></th>
                            <th><?= __('Mm Tk 0') ?></th>
                            <th><?= __('Slot') ?></th>
                            <th><?= __('Boot') ?></th>
                            <th><?= __('Boot Init') ?></th>
                            <th><?= __('Release Version') ?></th>
                            <th><?= __('Act') ?></th>
                            <th><?= __('On Service') ?></th>
                            <th><?= __('Off Service') ?></th>
                            <th><?= __('Client Id') ?></th>
                            <th><?= __('Service Type Id') ?></th>
                            <th><?= __('Comment') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($serviceType->mimias as $mimias) : ?>
                        <tr>
                            <td><?= h($mimias->id) ?></td>
                            <td><?= h($mimias->mimia_tag) ?></td>
                            <td><?= h($mimias->mm_mac) ?></td>
                            <td><?= h($mimias->mac) ?></td>
                            <td><?= h($mimias->mm_tk_0) ?></td>
                            <td><?= h($mimias->slot) ?></td>
                            <td><?= h($mimias->boot) ?></td>
                            <td><?= h($mimias->boot_init) ?></td>
                            <td><?= h($mimias->release_version) ?></td>
                            <td><?= h($mimias->act) ?></td>
                            <td><?= h($mimias->on_service) ?></td>
                            <td><?= h($mimias->off_service) ?></td>
                            <td><?= h($mimias->client_id) ?></td>
                            <td><?= h($mimias->service_type_id) ?></td>
                            <td><?= h($mimias->comment) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Mimias', 'action' => 'view', $mimias->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Mimias', 'action' => 'edit', $mimias->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Mimias', 'action' => 'delete', $mimias->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mimias->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
