<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Mimia> $mimias
 */
?>
<div class="mimias index content">
    <?= $this->Html->link(__('New Mimia'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Mimias') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('mimia_tag') ?></th>
                    <th><?= $this->Paginator->sort('mm_mac') ?></th>
                    <th><?= $this->Paginator->sort('mac') ?></th>
                    <th><?= $this->Paginator->sort('mm_tk_0') ?></th>
                    <th><?= $this->Paginator->sort('slot') ?></th>
                    <th><?= $this->Paginator->sort('boot') ?></th>
                    <th><?= $this->Paginator->sort('boot_init') ?></th>
                    <th><?= $this->Paginator->sort('release_version') ?></th>
                    <th><?= $this->Paginator->sort('act') ?></th>
                    <th><?= $this->Paginator->sort('on_service') ?></th>
                    <th><?= $this->Paginator->sort('off_service') ?></th>
                    <th><?= $this->Paginator->sort('client_id') ?></th>
                    <th><?= $this->Paginator->sort('service_type_id') ?></th>
                    <th><?= $this->Paginator->sort('comment') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($mimias as $mimia): ?>
                <tr>
                    <td><?= $this->Number->format($mimia->id) ?></td>
                    <td><?= $this->Number->format($mimia->mimia_tag) ?></td>
                    <td><?= $this->Number->format($mimia->mm_mac) ?></td>
                    <td><?= h($mimia->mac) ?></td>
                    <td><?= $this->Number->format($mimia->mm_tk_0) ?></td>
                    <td><?= $this->Number->format($mimia->slot) ?></td>
                    <td><?= h($mimia->boot) ?></td>
                    <td><?= h($mimia->boot_init) ?></td>
                    <td><?= h($mimia->release_version) ?></td>
                    <td><?= h($mimia->act) ?></td>
                    <td><?= h($mimia->on_service) ?></td>
                    <td><?= h($mimia->off_service) ?></td>
                    <td><?= $mimia->has('client') ? $this->Html->link($mimia->client->name, ['controller' => 'Clients', 'action' => 'view', $mimia->client->id]) : '' ?></td>
                    <td><?= $this->Number->format($mimia->service_type_id) ?></td>
                    <td><?= h($mimia->comment) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $mimia->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mimia->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mimia->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mimia->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
