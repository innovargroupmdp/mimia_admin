<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Mimia $mimia
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Mimia'), ['action' => 'edit', $mimia->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Mimia'), ['action' => 'delete', $mimia->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mimia->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Mimias'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Mimia'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="mimias view content">
            <h3><?= h($mimia->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Mac') ?></th>
                    <td><?= h($mimia->mac) ?></td>
                </tr>
                <tr>
                    <th><?= __('Release Version') ?></th>
                    <td><?= h($mimia->release_version) ?></td>
                </tr>
                <tr>
                    <th><?= __('Client') ?></th>
                    <td><?= $mimia->has('client') ? $this->Html->link($mimia->client->name, ['controller' => 'Clients', 'action' => 'view', $mimia->client->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Comment') ?></th>
                    <td><?= h($mimia->comment) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($mimia->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Mimia Tag') ?></th>
                    <td><?= $this->Number->format($mimia->mimia_tag) ?></td>
                </tr>
                <tr>
                    <th><?= __('Mm Mac') ?></th>
                    <td><?= $this->Number->format($mimia->mm_mac) ?></td>
                </tr>
                <tr>
                    <th><?= __('Mm Tk 0') ?></th>
                    <td><?= $this->Number->format($mimia->mm_tk_0) ?></td>
                </tr>
                <tr>
                    <th><?= __('Slot') ?></th>
                    <td><?= $this->Number->format($mimia->slot) ?></td>
                </tr>
                <tr>
                    <th><?= __('Service Type Id') ?></th>
                    <td><?= $this->Number->format($mimia->service_type_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('On Service') ?></th>
                    <td><?= h($mimia->on_service) ?></td>
                </tr>
                <tr>
                    <th><?= __('Off Service') ?></th>
                    <td><?= h($mimia->off_service) ?></td>
                </tr>
                <tr>
                    <th><?= __('Boot') ?></th>
                    <td><?= $mimia->boot ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th><?= __('Boot Init') ?></th>
                    <td><?= $mimia->boot_init ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th><?= __('Act') ?></th>
                    <td><?= $mimia->act ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Devices') ?></h4>
                <?php if (!empty($mimia->devices)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Dvc Tag') ?></th>
                            <th><?= __('Tk Mm Sr') ?></th>
                            <th><?= __('Tk Mm') ?></th>
                            <th><?= __('Act') ?></th>
                            <th><?= __('Boot') ?></th>
                            <th><?= __('Mimia Id') ?></th>
                            <th><?= __('Comment') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($mimia->devices as $devices) : ?>
                        <tr>
                            <td><?= h($devices->id) ?></td>
                            <td><?= h($devices->dvc_tag) ?></td>
                            <td><?= h($devices->tk_mm_sr) ?></td>
                            <td><?= h($devices->tk_mm) ?></td>
                            <td><?= h($devices->act) ?></td>
                            <td><?= h($devices->boot) ?></td>
                            <td><?= h($devices->mimia_id) ?></td>
                            <td><?= h($devices->comment) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Devices', 'action' => 'view', $devices->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Devices', 'action' => 'edit', $devices->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Devices', 'action' => 'delete', $devices->id], ['confirm' => __('Are you sure you want to delete # {0}?', $devices->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Measures') ?></h4>
                <?php if (!empty($mimia->measures)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Slot') ?></th>
                            <th><?= __('Trie N') ?></th>
                            <th><?= __('Tx Power') ?></th>
                            <th><?= __('Noise Lv') ?></th>
                            <th><?= __('Rx Signal') ?></th>
                            <th><?= __('Battery Lv') ?></th>
                            <th><?= __('Current Tx Date') ?></th>
                            <th><?= __('Current Measure') ?></th>
                            <th><?= __('Current Measure Date') ?></th>
                            <th><?= __('Measure Dif File') ?></th>
                            <th><?= __('Mimia Id') ?></th>
                            <th><?= __('Gateway Id') ?></th>
                            <th><?= __('Comment') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($mimia->measures as $measures) : ?>
                        <tr>
                            <td><?= h($measures->id) ?></td>
                            <td><?= h($measures->slot) ?></td>
                            <td><?= h($measures->trie_n) ?></td>
                            <td><?= h($measures->tx_power) ?></td>
                            <td><?= h($measures->noise_lv) ?></td>
                            <td><?= h($measures->rx_signal) ?></td>
                            <td><?= h($measures->battery_lv) ?></td>
                            <td><?= h($measures->current_tx_date) ?></td>
                            <td><?= h($measures->current_measure) ?></td>
                            <td><?= h($measures->current_measure_date) ?></td>
                            <td><?= h($measures->measure_dif_file) ?></td>
                            <td><?= h($measures->mimia_id) ?></td>
                            <td><?= h($measures->gateway_id) ?></td>
                            <td><?= h($measures->comment) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Measures', 'action' => 'view', $measures->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Measures', 'action' => 'edit', $measures->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Measures', 'action' => 'delete', $measures->id], ['confirm' => __('Are you sure you want to delete # {0}?', $measures->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
