<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Mimia $mimia
 * @var \Cake\Collection\CollectionInterface|string[] $clients
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Mimias'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="mimias form content">
            <?= $this->Form->create($mimia) ?>
            <fieldset>
                <legend><?= __('Add Mimia') ?></legend>
                <?php
                    echo $this->Form->control('mimia_tag');
                    echo $this->Form->control('mm_mac');
                    echo $this->Form->control('mac');
                    echo $this->Form->control('mm_tk_0');
                    echo $this->Form->control('slot');
                    echo $this->Form->control('boot');
                    echo $this->Form->control('boot_init');
                    echo $this->Form->control('release_version');
                    echo $this->Form->control('act');
                    echo $this->Form->control('on_service', ['empty' => true]);
                    echo $this->Form->control('off_service', ['empty' => true]);
                    echo $this->Form->control('client_id', ['options' => $clients]);
                    echo $this->Form->control('service_type_id');
                    echo $this->Form->control('comment');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
