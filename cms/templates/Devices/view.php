<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Device $device
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Device'), ['action' => 'edit', $device->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Device'), ['action' => 'delete', $device->id], ['confirm' => __('Are you sure you want to delete # {0}?', $device->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Devices'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Device'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="devices view content">
            <h3><?= h($device->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Mimia') ?></th>
                    <td><?= $device->has('mimia') ? $this->Html->link($device->mimia->id, ['controller' => 'Mimias', 'action' => 'view', $device->mimia->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Comment') ?></th>
                    <td><?= h($device->comment) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($device->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Dvc Tag') ?></th>
                    <td><?= $this->Number->format($device->dvc_tag) ?></td>
                </tr>
                <tr>
                    <th><?= __('Tk Mm Sr') ?></th>
                    <td><?= $this->Number->format($device->tk_mm_sr) ?></td>
                </tr>
                <tr>
                    <th><?= __('Tk Mm') ?></th>
                    <td><?= $this->Number->format($device->tk_mm) ?></td>
                </tr>
                <tr>
                    <th><?= __('Act') ?></th>
                    <td><?= $device->act ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th><?= __('Boot') ?></th>
                    <td><?= $device->boot ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Control Datas') ?></h4>
                <?php if (!empty($device->control_datas)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Subtype') ?></th>
                            <th><?= __('Data Ctrl File') ?></th>
                            <th><?= __('Rx Tx') ?></th>
                            <th><?= __('Order Date') ?></th>
                            <th><?= __('Device Id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($device->control_datas as $controlDatas) : ?>
                        <tr>
                            <td><?= h($controlDatas->id) ?></td>
                            <td><?= h($controlDatas->subtype) ?></td>
                            <td><?= h($controlDatas->data_ctrl_file) ?></td>
                            <td><?= h($controlDatas->rx_tx) ?></td>
                            <td><?= h($controlDatas->order_date) ?></td>
                            <td><?= h($controlDatas->device_id) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'ControlDatas', 'action' => 'view', $controlDatas->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'ControlDatas', 'action' => 'edit', $controlDatas->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'ControlDatas', 'action' => 'delete', $controlDatas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $controlDatas->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
