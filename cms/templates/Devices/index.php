<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Device> $devices
 */
?>
<div class="devices index content">
    <?= $this->Html->link(__('New Device'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Devices') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('dvc_tag') ?></th>
                    <th><?= $this->Paginator->sort('tk_mm_sr') ?></th>
                    <th><?= $this->Paginator->sort('tk_mm') ?></th>
                    <th><?= $this->Paginator->sort('act') ?></th>
                    <th><?= $this->Paginator->sort('boot') ?></th>
                    <th><?= $this->Paginator->sort('mimia_id') ?></th>
                    <th><?= $this->Paginator->sort('comment') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($devices as $device): ?>
                <tr>
                    <td><?= $this->Number->format($device->id) ?></td>
                    <td><?= $this->Number->format($device->dvc_tag) ?></td>
                    <td><?= $this->Number->format($device->tk_mm_sr) ?></td>
                    <td><?= $this->Number->format($device->tk_mm) ?></td>
                    <td><?= h($device->act) ?></td>
                    <td><?= h($device->boot) ?></td>
                    <td><?= $device->has('mimia') ? $this->Html->link($device->mimia->id, ['controller' => 'Mimias', 'action' => 'view', $device->mimia->id]) : '' ?></td>
                    <td><?= h($device->comment) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $device->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $device->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $device->id], ['confirm' => __('Are you sure you want to delete # {0}?', $device->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
