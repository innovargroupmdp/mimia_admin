<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Plan $plan
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Plan'), ['action' => 'edit', $plan->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Plan'), ['action' => 'delete', $plan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $plan->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Plans'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Plan'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="plans view content">
            <h3><?= h($plan->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Type') ?></th>
                    <td><?= h($plan->type) ?></td>
                </tr>
                <tr>
                    <th><?= __('Worth') ?></th>
                    <td><?= h($plan->worth) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($plan->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Mimias') ?></th>
                    <td><?= $this->Number->format($plan->mimias) ?></td>
                </tr>
                <tr>
                    <th><?= __('Gateways') ?></th>
                    <td><?= $this->Number->format($plan->gateways) ?></td>
                </tr>
                <tr>
                    <th><?= __('Portal') ?></th>
                    <td><?= $this->Number->format($plan->portal) ?></td>
                </tr>
                <tr>
                    <th><?= __('Api') ?></th>
                    <td><?= $this->Number->format($plan->api) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Clients') ?></h4>
                <?php if (!empty($plan->clients)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Client Tag') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Vat') ?></th>
                            <th><?= __('Company Type Id') ?></th>
                            <th><?= __('Web') ?></th>
                            <th><?= __('Address') ?></th>
                            <th><?= __('Country') ?></th>
                            <th><?= __('Plan Id') ?></th>
                            <th><?= __('Billing Id') ?></th>
                            <th><?= __('User') ?></th>
                            <th><?= __('Password') ?></th>
                            <th><?= __('State Id') ?></th>
                            <th><?= __('Contact') ?></th>
                            <th><?= __('Email') ?></th>
                            <th><?= __('Phone') ?></th>
                            <th><?= __('Act') ?></th>
                            <th><?= __('Enroll') ?></th>
                            <th><?= __('Down') ?></th>
                            <th><?= __('Logo') ?></th>
                            <th><?= __('Note') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($plan->clients as $clients) : ?>
                        <tr>
                            <td><?= h($clients->id) ?></td>
                            <td><?= h($clients->client_tag) ?></td>
                            <td><?= h($clients->name) ?></td>
                            <td><?= h($clients->vat) ?></td>
                            <td><?= h($clients->company_type_id) ?></td>
                            <td><?= h($clients->web) ?></td>
                            <td><?= h($clients->address) ?></td>
                            <td><?= h($clients->country) ?></td>
                            <td><?= h($clients->plan_id) ?></td>
                            <td><?= h($clients->billing_id) ?></td>
                            <td><?= h($clients->user) ?></td>
                            <td><?= h($clients->password) ?></td>
                            <td><?= h($clients->state_id) ?></td>
                            <td><?= h($clients->contact) ?></td>
                            <td><?= h($clients->email) ?></td>
                            <td><?= h($clients->phone) ?></td>
                            <td><?= h($clients->act) ?></td>
                            <td><?= h($clients->enroll) ?></td>
                            <td><?= h($clients->down) ?></td>
                            <td><?= h($clients->logo) ?></td>
                            <td><?= h($clients->note) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Clients', 'action' => 'view', $clients->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Clients', 'action' => 'edit', $clients->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Clients', 'action' => 'delete', $clients->id], ['confirm' => __('Are you sure you want to delete # {0}?', $clients->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
