<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;


use Authentication\PasswordHasher\DefaultPasswordHasher;

use Authentication\IdentityInterface;

/**
 * Client Entity
 *
 * @property int $id
 * @property int $client_tag
 * @property string $name
 * @property string $vat
 * @property bool $company_type_id
 * @property string|null $web
 * @property string|null $address
 * @property string $country
 * @property int|null $plan_id
 * @property bool|null $billing_id
 * @property string|null $user
 * @property string|null $password
 * @property bool $state_id
 * @property string $contact
 * @property string $email
 * @property string $phone
 * @property bool|null $act
 * @property \Cake\I18n\FrozenTime $enroll
 * @property \Cake\I18n\FrozenTime|null $down
 * @property string|null $logo
 * @property string|null $note
 *
 * @property \App\Model\Entity\Plan $plan
 * @property \App\Model\Entity\Billing $billing
 * @property \App\Model\Entity\State $state
 * @property \App\Model\Entity\Gateway[] $gateways
 * @property \App\Model\Entity\Mimia[] $mimias
 * @property \App\Model\Entity\User[] $users
 */
class Client extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
     protected $_accessible = [
        'client_tag' => true,
        'name' => true,
        'vat' => true,
        'company_type_id' => true,
        'web' => true,
        'address' => true,
        'country' => true,
        'plan_id' => true,
        'billing_id' => true,
        'user' => true,
        'password' => true,
        'state_id' => true,
        'contact' => true,
        'email' => true,
        'phone' => true,
        'act' => true,
        'enroll' => true,
        'down' => true,
        'logo' => true,
        'note' => true,
        'company_type' => true,
        'plan' => true,
        'billing' => true,
        'state' => true,
        'gateways' => true,
        'mimias' => true,
        'users' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array<string>
     */
    protected $_hidden = [
        'password',
    ];


    /**
   *                  My Owm Code
   * 
   */

    

    // Automatically hash passwords when they are changed.
    protected function _setPassword(string $password)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($password);
    }
    /**
     * Authentication\IdentityInterface method
     */
    public function getIdentifier()
    {
        return $this->id;
    }

    /**
     * Authentication\IdentityInterface method
     */
    public function getOriginalData()
    {
      return $this;
    }    
}
