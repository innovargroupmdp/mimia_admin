<?php
declare(strict_types=1);

namespace App\Model\Entity;
use Cake\ORM\Entity;
use Authentication\PasswordHasher\DefaultPasswordHasher;

use Authentication\IdentityInterface;
/**
 * User Entity
 *
 * @property int $id
 * @property string $user
 * @property string $email
 * @property string $password
 * @property int $role_id
 * @property int $client_id
 * @property string|null $avatar
 * @property bool|null $act
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenDate $last
 * @property bool $logged
 * @property string $name
 * @property string|null $note
 *
 * @property \App\Model\Entity\Role $role
 * @property \App\Model\Entity\Client $client
 */
class User extends Entity implements IdentityInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'user' => true,
        'email' => true,
        'password' => true,
        'role_id' => true,
        'client_id' => true,
        'avatar' => true,
        'act' => true,
        'created' => true,
        'modified' => true,
        'last' => true,
        'logged' => true,
        'name' => true,
        'note' => true,
        'role' => true,
        'client' => true,
    ];

   /**
   *                  My Owm Code
   * 
   */

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array<string>
     */
    protected $_hidden = [
        'password',
    ];

    // Automatically hash passwords when they are changed.
    protected function _setPassword(string $password)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($password);
    }
    /**
     * Authentication\IdentityInterface method
     */
    public function getIdentifier()
    {
        return $this->id;
    }

    /**
     * Authentication\IdentityInterface method
     */
    public function getOriginalData()
    {
      return $this;
    }    
}
