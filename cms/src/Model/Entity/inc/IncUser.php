<?php

use Cake\Auth\DefaultPasswordHasher;

protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
          return (new DefaultPasswordHasher)->hash($password);
        }
    }

?>    