<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Gateway Entity
 *
 * @property int $id
 * @property int $gw_tag
 * @property int $gw_mac
 * @property string $mac
 * @property string $key_svr0
 * @property string|null $key_svr
 * @property bool $boot
 * @property bool $boot_Init
 * @property string $release_version
 * @property int $model_id
 * @property bool $act
 * @property \Cake\I18n\FrozenDate|null $on_service
 * @property \Cake\I18n\FrozenDate|null $off_service
 * @property int $client_id
 * @property string|null $comment
 * @property string|null $gis
 *
 * @property \App\Model\Entity\Client $client
 * @property \App\Model\Entity\Measure[] $measures
 */
class Gateway extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'gw_tag' => true,
        'gw_mac' => true,
        'mac' => true,
        'key_svr0' => true,
        'key_svr' => true,
        'boot' => true,
        'boot_Init' => true,
        'release_version' => true,
        'model_id' => true,
        'act' => true,
        'on_service' => true,
        'off_service' => true,
        'client_id' => true,
        'comment' => true,
        'gis' => true,
        'client' => true,
        'measures' => true,
    ];
}
