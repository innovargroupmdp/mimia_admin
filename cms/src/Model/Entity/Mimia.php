<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Mimia Entity
 *
 * @property int $id
 * @property int $mimia_tag
 * @property int $mm_mac
 * @property string $mac
 * @property int $mm_tk_0
 * @property int $slot
 * @property bool $boot
 * @property bool $boot_init
 * @property string $release_version
 * @property bool $act
 * @property \Cake\I18n\FrozenDate|null $on_service
 * @property \Cake\I18n\FrozenDate|null $off_service
 * @property int $client_id
 * @property int $service_type_id
 * @property string|null $comment
 *
 * @property \App\Model\Entity\Client $client
 * @property \App\Model\Entity\Device[] $devices
 * @property \App\Model\Entity\Measure[] $measures
 */
class Mimia extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'mimia_tag' => true,
        'mm_mac' => true,
        'mac' => true,
        'mm_tk_0' => true,
        'slot' => true,
        'boot' => true,
        'boot_init' => true,
        'release_version' => true,
        'act' => true,
        'on_service' => true,
        'off_service' => true,
        'client_id' => true,
        'service_type_id' => true,
        'comment' => true,
        'client' => true,
        'devices' => true,
        'measures' => true,
    ];
}
