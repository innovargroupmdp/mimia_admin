<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Measure Entity
 *
 * @property int $id
 * @property int $slot
 * @property int $trie_n
 * @property int $tx_power
 * @property int $noise_lv
 * @property int $rx_signal
 * @property string $battery_lv
 * @property \Cake\I18n\Time $current_tx_date
 * @property int $current_measure
 * @property \Cake\I18n\FrozenDate $current_measure_date
 * @property string|null $measure_dif_file
 * @property int $mimia_id
 * @property int $gateway_id
 * @property string|null $comment
 *
 * @property \App\Model\Entity\Mimia $mimia
 * @property \App\Model\Entity\Gateway $gateway
 */
class Measure extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'slot' => true,
        'trie_n' => true,
        'tx_power' => true,
        'noise_lv' => true,
        'rx_signal' => true,
        'battery_lv' => true,
        'current_tx_date' => true,
        'current_measure' => true,
        'current_measure_date' => true,
        'measure_dif_file' => true,
        'mimia_id' => true,
        'gateway_id' => true,
        'comment' => true,
        'mimia' => true,
        'gateway' => true,
    ];
}
