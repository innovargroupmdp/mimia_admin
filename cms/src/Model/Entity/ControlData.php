<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ControlData Entity
 *
 * @property int $id
 * @property int $subtype
 * @property string|null $data_ctrl_file
 * @property bool $rx_tx
 * @property \Cake\I18n\FrozenTime $order_date
 * @property int $device_id
 *
 * @property \App\Model\Entity\Device $device
 */
class ControlData extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'subtype' => true,
        'data_ctrl_file' => true,
        'rx_tx' => true,
        'order_date' => true,
        'device_id' => true,
        'device' => true,
    ];
}
