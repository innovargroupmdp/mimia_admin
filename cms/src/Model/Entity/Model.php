<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Model Entity
 *
 * @property int $id
 * @property int $device_type_id
 * @property string $release_version
 * @property string $description
 * @property bool $producction
 * @property string $picture
 *
 * @property \App\Model\Entity\DeviceType $device_type
 * @property \App\Model\Entity\Gateway[] $gateways
 */
class Model extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'device_type_id' => true,
        'release_version' => true,
        'description' => true,
        'producction' => true,
        'picture' => true,
        'device_type' => true,
        'gateways' => true,
    ];
}
