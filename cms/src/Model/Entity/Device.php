<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Device Entity
 *
 * @property int $id
 * @property int $dvc_tag
 * @property int $tk_mm_sr
 * @property int $tk_mm
 * @property bool $act
 * @property bool $boot
 * @property int $mimia_id
 * @property string|null $comment
 *
 * @property \App\Model\Entity\Mimia $mimia
 * @property \App\Model\Entity\ControlData[] $control_datas
 */
class Device extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'dvc_tag' => true,
        'tk_mm_sr' => true,
        'tk_mm' => true,
        'act' => true,
        'boot' => true,
        'mimia_id' => true,
        'comment' => true,
        'mimia' => true,
        'control_datas' => true,
    ];
}
