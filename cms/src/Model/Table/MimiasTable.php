<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Mimias Model
 *
 * @property \App\Model\Table\ClientsTable&\Cake\ORM\Association\BelongsTo $Clients
 * @property \App\Model\Table\DevicesTable&\Cake\ORM\Association\HasMany $Devices
 * @property \App\Model\Table\MeasuresTable&\Cake\ORM\Association\HasMany $Measures
 *
 * @method \App\Model\Entity\Mimia newEmptyEntity()
 * @method \App\Model\Entity\Mimia newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Mimia[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Mimia get($primaryKey, $options = [])
 * @method \App\Model\Entity\Mimia findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Mimia patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Mimia[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Mimia|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Mimia saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Mimia[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Mimia[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Mimia[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Mimia[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MimiasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('mimias');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ServiceTypes', [
            'foreignKey' => 'service_type_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Devices', [
            'foreignKey' => 'mimia_id',
        ]);
        $this->hasMany('Measures', [
            'foreignKey' => 'mimia_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('mimia_tag')
            ->requirePresence('mimia_tag', 'create')
            ->notEmptyString('mimia_tag');

        $validator
            ->nonNegativeInteger('mm_mac')
            ->requirePresence('mm_mac', 'create')
            ->notEmptyString('mm_mac');

        $validator
            ->scalar('mac')
            ->maxLength('mac', 23)
            ->requirePresence('mac', 'create')
            ->notEmptyString('mac')
            ->add('mac', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->nonNegativeInteger('mm_tk_0')
            ->requirePresence('mm_tk_0', 'create')
            ->notEmptyString('mm_tk_0');

        $validator
            ->requirePresence('slot', 'create')
            ->notEmptyString('slot');

        $validator
            ->boolean('boot')
            ->notEmptyString('boot');

        $validator
            ->boolean('boot_init')
            ->notEmptyString('boot_init');

        $validator
            ->scalar('release_version')
            ->maxLength('release_version', 255)
            ->requirePresence('release_version', 'create')
            ->notEmptyString('release_version');

        $validator
            ->boolean('act')
            ->notEmptyString('act');

        $validator
            ->date('on_service')
            ->allowEmptyDate('on_service');

        $validator
            ->date('off_service')
            ->allowEmptyDate('off_service');

        $validator
            ->nonNegativeInteger('client_id')
            ->notEmptyString('client_id');

        $validator
            ->integer('service_type_id')
            ->notEmptyString('service_type_id');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 255)
            ->allowEmptyString('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['mac']), ['errorField' => 'mac']);
        $rules->add($rules->existsIn('client_id', 'Clients'), ['errorField' => 'client_id']);
        $rules->add($rules->existsIn('service_type_id', 'ServiceTypes'), ['errorField' => 'service_type_id']);

        return $rules;
    }
}
