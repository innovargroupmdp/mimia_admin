<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Gateways Model
 *
 * @property \App\Model\Table\ClientsTable&\Cake\ORM\Association\BelongsTo $Clients
 * @property \App\Model\Table\MeasuresTable&\Cake\ORM\Association\HasMany $Measures
 *
 * @method \App\Model\Entity\Gateway newEmptyEntity()
 * @method \App\Model\Entity\Gateway newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Gateway[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Gateway get($primaryKey, $options = [])
 * @method \App\Model\Entity\Gateway findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Gateway patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Gateway[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Gateway|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Gateway saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Gateway[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Gateway[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Gateway[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Gateway[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class GatewaysTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('gateways');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Models', [
            'foreignKey' => 'model_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Measures', [
            'foreignKey' => 'gateway_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('gw_tag')
            ->requirePresence('gw_tag', 'create')
            ->notEmptyString('gw_tag');

        $validator
            ->nonNegativeInteger('gw_mac')
            ->requirePresence('gw_mac', 'create')
            ->notEmptyString('gw_mac');

        $validator
            ->scalar('mac')
            ->maxLength('mac', 23)
            ->requirePresence('mac', 'create')
            ->notEmptyString('mac')
            ->add('mac', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('key_svr0')
            ->maxLength('key_svr0', 11)
            ->requirePresence('key_svr0', 'create')
            ->notEmptyString('key_svr0');

        $validator
            ->scalar('key_svr')
            ->maxLength('key_svr', 11)
            ->allowEmptyString('key_svr');

        $validator
            ->boolean('boot')
            ->notEmptyString('boot');

        $validator
            ->boolean('boot_Init')
            ->notEmptyString('boot_Init');

        $validator
            ->scalar('release_version')
            ->maxLength('release_version', 10)
            ->requirePresence('release_version', 'create')
            ->notEmptyString('release_version');

        $validator
            ->nonNegativeInteger('model_id')
            ->notEmptyString('model_id');

        $validator
            ->boolean('act')
            ->notEmptyString('act');

        $validator
            ->date('on_service')
            ->allowEmptyDate('on_service');

        $validator
            ->date('off_service')
            ->allowEmptyDate('off_service');

        $validator
            ->nonNegativeInteger('client_id')
            ->notEmptyString('client_id');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 255)
            ->allowEmptyString('comment');

        $validator
            ->scalar('gis')
            ->maxLength('gis', 100)
            ->allowEmptyString('gis');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['mac']), ['errorField' => 'mac']);
        $rules->add($rules->existsIn('model_id', 'Models'), ['errorField' => 'model_id']);
        $rules->add($rules->existsIn('client_id', 'Clients'), ['errorField' => 'client_id']);

        return $rules;
    }
}
