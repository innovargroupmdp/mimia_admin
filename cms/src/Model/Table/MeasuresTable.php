<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Measures Model
 *
 * @property \App\Model\Table\MimiasTable&\Cake\ORM\Association\BelongsTo $Mimias
 * @property \App\Model\Table\GatewaysTable&\Cake\ORM\Association\BelongsTo $Gateways
 *
 * @method \App\Model\Entity\Measure newEmptyEntity()
 * @method \App\Model\Entity\Measure newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Measure[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Measure get($primaryKey, $options = [])
 * @method \App\Model\Entity\Measure findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Measure patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Measure[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Measure|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Measure saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Measure[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Measure[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Measure[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Measure[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MeasuresTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('measures');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Mimias', [
            'foreignKey' => 'mimia_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Gateways', [
            'foreignKey' => 'gateway_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->requirePresence('slot', 'create')
            ->notEmptyString('slot');

        $validator
            ->requirePresence('trie_n', 'create')
            ->notEmptyString('trie_n');

        $validator
            ->requirePresence('tx_power', 'create')
            ->notEmptyString('tx_power');

        $validator
            ->requirePresence('noise_lv', 'create')
            ->notEmptyString('noise_lv');

        $validator
            ->requirePresence('rx_signal', 'create')
            ->notEmptyString('rx_signal');

        $validator
            ->decimal('battery_lv')
            ->greaterThanOrEqual('battery_lv', 0)
            ->requirePresence('battery_lv', 'create')
            ->notEmptyString('battery_lv');

        $validator
            ->time('current_tx_date')
            ->requirePresence('current_tx_date', 'create')
            ->notEmptyTime('current_tx_date');

        $validator
            ->nonNegativeInteger('current_measure')
            ->requirePresence('current_measure', 'create')
            ->notEmptyString('current_measure');

        $validator
            ->date('current_measure_date')
            ->requirePresence('current_measure_date', 'create')
            ->notEmptyDate('current_measure_date');

        $validator
            ->scalar('measure_dif_file')
            ->maxLength('measure_dif_file', 255)
            ->allowEmptyFile('measure_dif_file');

        $validator
            ->nonNegativeInteger('mimia_id')
            ->notEmptyString('mimia_id');

        $validator
            ->nonNegativeInteger('gateway_id')
            ->notEmptyString('gateway_id');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 255)
            ->allowEmptyString('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('mimia_id', 'Mimias'), ['errorField' => 'mimia_id']);
        $rules->add($rules->existsIn('gateway_id', 'Gateways'), ['errorField' => 'gateway_id']);

        return $rules;
    }
}
