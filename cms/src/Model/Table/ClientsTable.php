<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Clients Model
 *
 * @property \App\Model\Table\CompanyTypesTable&\Cake\ORM\Association\BelongsTo $CompanyTypes
 * @property \App\Model\Table\PlansTable&\Cake\ORM\Association\BelongsTo $Plans
 * @property \App\Model\Table\BillingsTable&\Cake\ORM\Association\BelongsTo $Billings
 * @property \App\Model\Table\StatesTable&\Cake\ORM\Association\BelongsTo $States
 * @property \App\Model\Table\GatewaysTable&\Cake\ORM\Association\HasMany $Gateways
 * @property \App\Model\Table\MimiasTable&\Cake\ORM\Association\HasMany $Mimias
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\HasMany $Users
 *
 * @method \App\Model\Entity\Client newEmptyEntity()
 * @method \App\Model\Entity\Client newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Client[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Client get($primaryKey, $options = [])
 * @method \App\Model\Entity\Client findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Client patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Client[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Client|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Client saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Client[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Client[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Client[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Client[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ClientsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('clients');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('CompanyTypes', [
            'foreignKey' => 'company_type_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Plans', [
            'foreignKey' => 'plan_id',
        ]);
        $this->belongsTo('Billings', [
            'foreignKey' => 'billing_id',
        ]);
        $this->belongsTo('States', [
            'foreignKey' => 'state_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Gateways', [
            'foreignKey' => 'client_id',
        ]);
        $this->hasMany('Mimias', [
            'foreignKey' => 'client_id',
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'client_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('client_tag')
            ->requirePresence('client_tag', 'create')
            ->notEmptyString('client_tag')
            ->add('client_tag', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('vat')
            ->maxLength('vat', 15)
            ->requirePresence('vat', 'create')
            ->notEmptyString('vat');

        $validator
            ->notEmptyString('company_type_id');

        $validator
            ->scalar('web')
            ->maxLength('web', 255)
            ->allowEmptyString('web');

        $validator
            ->scalar('address')
            ->maxLength('address', 255)
            ->allowEmptyString('address');

        $validator
            ->scalar('country')
            ->maxLength('country', 255)
            ->requirePresence('country', 'create')
            ->notEmptyString('country');

        $validator
            ->allowEmptyString('plan_id');

        $validator
            ->allowEmptyString('billing_id');

        $validator
            ->scalar('user')
            ->maxLength('user', 255)
            ->allowEmptyString('user')
            ->add('user', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->allowEmptyString('password');

        $validator
            ->notEmptyString('state_id');

        $validator
            ->scalar('contact')
            ->maxLength('contact', 50)
            ->requirePresence('contact', 'create')
            ->notEmptyString('contact');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 15)
            ->requirePresence('phone', 'create')
            ->notEmptyString('phone');

        $validator
            ->boolean('act')
            ->allowEmptyString('act');

        $validator
            ->dateTime('enroll')
            ->notEmptyDateTime('enroll');

        $validator
            ->dateTime('down')
            ->allowEmptyDateTime('down');

        $validator
            ->scalar('logo')
            ->maxLength('logo', 255)
            ->allowEmptyString('logo');

        $validator
            ->scalar('note')
            ->maxLength('note', 255)
            ->allowEmptyString('note');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['client_tag']), ['errorField' => 'client_tag']);
        $rules->add($rules->isUnique(['user'], ['allowMultipleNulls' => true]), ['errorField' => 'user']);
        $rules->add($rules->existsIn('company_type_id', 'CompanyTypes'), ['errorField' => 'company_type_id']);
        $rules->add($rules->existsIn('plan_id', 'Plans'), ['errorField' => 'plan_id']);
        $rules->add($rules->existsIn('billing_id', 'Billings'), ['errorField' => 'billing_id']);
        $rules->add($rules->existsIn('state_id', 'States'), ['errorField' => 'state_id']);

        return $rules;
    }
}
