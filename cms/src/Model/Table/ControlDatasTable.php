<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ControlDatas Model
 *
 * @property \App\Model\Table\DevicesTable&\Cake\ORM\Association\BelongsTo $Devices
 *
 * @method \App\Model\Entity\ControlData newEmptyEntity()
 * @method \App\Model\Entity\ControlData newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ControlData[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ControlData get($primaryKey, $options = [])
 * @method \App\Model\Entity\ControlData findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ControlData patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ControlData[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ControlData|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ControlData saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ControlData[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ControlData[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ControlData[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ControlData[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ControlDatasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('control_datas');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Devices', [
            'foreignKey' => 'device_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->requirePresence('subtype', 'create')
            ->notEmptyString('subtype');

        $validator
            ->scalar('data_ctrl_file')
            ->maxLength('data_ctrl_file', 255)
            ->allowEmptyFile('data_ctrl_file');

        $validator
            ->boolean('rx_tx')
            ->requirePresence('rx_tx', 'create')
            ->notEmptyString('rx_tx');

        $validator
            ->dateTime('order_date')
            ->notEmptyDateTime('order_date');

        $validator
            ->nonNegativeInteger('device_id')
            ->notEmptyString('device_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('device_id', 'Devices'), ['errorField' => 'device_id']);

        return $rules;
    }
}
