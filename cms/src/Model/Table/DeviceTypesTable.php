<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DeviceTypes Model
 *
 * @property \App\Model\Table\ModelsTable&\Cake\ORM\Association\HasMany $Models
 *
 * @method \App\Model\Entity\DeviceType newEmptyEntity()
 * @method \App\Model\Entity\DeviceType newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\DeviceType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DeviceType get($primaryKey, $options = [])
 * @method \App\Model\Entity\DeviceType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\DeviceType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DeviceType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\DeviceType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DeviceType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DeviceType[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\DeviceType[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\DeviceType[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\DeviceType[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class DeviceTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('device_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Models', [
            'foreignKey' => 'device_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('type')
            ->maxLength('type', 50)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('description')
            ->maxLength('description', 500)
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->scalar('symbol')
            ->maxLength('symbol', 100)
            ->requirePresence('symbol', 'create')
            ->notEmptyString('symbol');

        return $validator;
    }
}
