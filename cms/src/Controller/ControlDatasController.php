<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * ControlDatas Controller
 *
 * @property \App\Model\Table\ControlDatasTable $ControlDatas
 * @method \App\Model\Entity\ControlData[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ControlDatasController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Devices'],
        ];
        $controlDatas = $this->paginate($this->ControlDatas);

        $this->set(compact('controlDatas'));
    }

    /**
     * View method
     *
     * @param string|null $id Control Data id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $controlData = $this->ControlDatas->get($id, [
            'contain' => ['Devices'],
        ]);

        $this->set(compact('controlData'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $controlData = $this->ControlDatas->newEmptyEntity();
        if ($this->request->is('post')) {
            $controlData = $this->ControlDatas->patchEntity($controlData, $this->request->getData());
            if ($this->ControlDatas->save($controlData)) {
                $this->Flash->success(__('The control data has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The control data could not be saved. Please, try again.'));
        }
        $devices = $this->ControlDatas->Devices->find('list', ['limit' => 200])->all();
        $this->set(compact('controlData', 'devices'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Control Data id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $controlData = $this->ControlDatas->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $controlData = $this->ControlDatas->patchEntity($controlData, $this->request->getData());
            if ($this->ControlDatas->save($controlData)) {
                $this->Flash->success(__('The control data has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The control data could not be saved. Please, try again.'));
        }
        $devices = $this->ControlDatas->Devices->find('list', ['limit' => 200])->all();
        $this->set(compact('controlData', 'devices'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Control Data id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $controlData = $this->ControlDatas->get($id);
        if ($this->ControlDatas->delete($controlData)) {
            $this->Flash->success(__('The control data has been deleted.'));
        } else {
            $this->Flash->error(__('The control data could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
