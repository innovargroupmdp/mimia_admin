<?php
declare(strict_types=1);

namespace App\Controller;

//use Authentication\IdentityInterface;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index($id = null)
    {
      /* *My Owm Code*/            
      $this->Authorization->skipAuthorization();

      $this->paginate = [
          'contain' => ['Roles', 'Clients'],
      ];
     
      $query  = $this->Users->find();
      $result = $query->where( ['client_id =' => $id] );

      $users  = $this->paginate($result);

      $Roles  = $this->fetchTable('Roles')->find()->all();

      $roles  = $this->keyvalue($Roles);
      $this->viewBuilder()->setLayout('template_main');
      $this->set(compact('users','roles'));

    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

      $user = $this->Users->get($id, [
            'contain' => ['Roles', 'Clients'],
        ]);    
      
      $this->Authorization->authorize($user);

      $this->viewBuilder()->setLayout('template_main');      
      $this->set(compact('user'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //TODO !!! validation data from the rigth role user, the added role user nerver whould be mayor that user logged.
        $user = $this->Users->newEmptyEntity();
        $this->Authorization->authorize($user);
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200])->all();
        $clients = $this->Users->Clients->find('list', ['limit' => 200])->all();
        $this->set(compact('user', 'roles', 'clients'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
      $user = $this->Users->get($id, [
            'contain' => [],
        ]);
      $this->Authorization->authorize($user);  
      if ($this->request->is(['patch', 'post', 'put'])) {
          $user = $this->Users->patchEntity($user, $this->request->getData());
          if ($this->Users->save($user)) {
              $this->Flash->success(__('The user has been saved.'));

              return $this->redirect(['action' => 'index']);
          }
          $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
      $roles = $this->Users->Roles->find('list', ['limit' => 200])->all();
      $clients = $this->Users->Clients->find('list', ['limit' => 200])->all();
      $this->set(compact('user', 'roles', 'clients'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

  /**
   *                  My Owm Code
   * 
  */

  /**
   * beforeFilter
   * enhable auth methods besides auth method in AppControler.php
   * deny everything lest login.
   *
   */  
  public function beforeFilter(\Cake\Event\EventInterface $event)
  {
    parent::beforeFilter($event);

    //1 $this->Auth->allow(['login','singup','forgotPassword']);
    //$this->Authentication->allowUnauthenticated(['login']);
    //$this->Authentication->allowUnauthenticated(['login','add','view', 'index']);
    $this->Authentication->allowUnauthenticated(['login','singup','forgotPassword']);

  }

  /**
   * home method
   *
   * @return \Cake\Http\Response|null|void Renders view
   */
  public function home()
  {
    
    /* *My Owm Code*/      
    $this->Authorization->skipAuthorization();    
    $this->viewBuilder()->setLayout('template_main');

    $this->paginate = [
        'contain' => ['Roles', 'Clients'],
    ];
    $users = $this->paginate($this->Users);

    //$this->Authorization->authorize($users);
      $this->set(compact('users'));
  }

  /**
   * Index method
   *
   * @return \Cake\Http\Response|null|void Renders view
   */
  public function detail()
  {
      $this->paginate = [
          'contain' => ['Roles', 'Clients'],
      ];
      $users = $this->paginate($this->Users);

    $this->Authorization->authorize($user);
      $this->set(compact('users'));
  }


  /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function setting($id = null)
    {
      $user = $this->Users->get($id, [
          'contain' => [],
      ]);
      $this->Authorization->authorize($user);  
      if ($this->request->is(['patch', 'post', 'put'])) {
        $user = $this->Users->patchEntity($user, $this->request->getData());
        if ($this->Users->save($user)) {
            $this->Flash->success(__('The user has been saved.'));

            //return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('The user could not be saved. Please, try again.'));
      }      
      $this->Authorization->authorize($user);
      $this->viewBuilder()->setLayout('template_main');      
      $this->set(compact('user'));
    }

  /**
   * login method    $this->viewBuilder()->setLayout('template_login');

   *
   * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
   */

  // in src/Controller/UsersController.php
  public function login(){    

    $this->viewBuilder()->setLayout('template_login');
    if ($this->request->is(['post','put'])) {
      // If the user not logged in.
      $result = $this->Authentication->getResult();
      if ($result->isValid()) {
        // The user is valid.
        $this->Flash->success(__('Login Successful.'));
        $target = $this->Authentication->getLoginRedirect() ?? ['action'=>'home'];
        return $this->redirect($target);
      }
      $this->Flash->error(__('Login Was Not Successful.'));
    }
    
    if ($this->Authentication->getIdentity()) {
      //The user is logged.
      // If the user is logged in send them away.
      $this->Flash->warning(__('You Are Already Logged.'));
      return $this->redirect(['controller' => 'Users','action'=>'home']);
    }
  }

  /**
   * singup method
   *
   * @param string|null $id User id.
   * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
   * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
   */
  public function singup($id = null){
      $user = $this->Users->get($id, [
          'contain' => [],
      ]);
      if ($this->request->is(['patch', 'post', 'put'])) {
          $user = $this->Users->patchEntity($user, $this->request->getData());
          if ($this->Users->save($user)) {
              $this->Flash->success(__('The user has been saved.'));
              return $this->redirect(['action' => 'index']);
          }
          $this->Flash->error(__('The user could not be saved. Please, try again.'));
      }
      $roles = $this->Users->Roles->find('list', ['limit' => 200])->all();
      $clients = $this->Users->Clients->find('list', ['limit' => 200])->all();
      $this->set(compact('user', 'roles', 'clients'));
  }

  /**
   * logout method
   *
   * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
   */
    public function logout()
  {
    
    $this->Authorization->skipAuthorization();
    $result = $this->Authentication->getResult();
    // regardless of POST or GET, redirect if user is logged in
    if ($result->isValid()) {
      $this->Authentication->logout();
      $this->Flash->success(__('You Are Logged Out.'));
      return $this->redirect(['controller' => 'Users', 'action' => 'login']);
      //return $this->redirect('http://localhost/Xampp/mimia_admin/');
    }      
  }

  /**
   * forgotPassword method
   *
   * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
   */
  public function forgotPassword()
  {
      retun;
  }

  /*public function login(){
    
    if ($this->request->is(['post','put'])) {
      $user = $this->Auth->identify();
      if ($user) {
        $this->Flash->success(__('The user is correct.'));
        $this->user->setUser();
        //redirect
        return $this->redirect(['Controller'=>'Users','action'=>'index']);
      }
      $this->Flash->error(__('The user or password could not be checked. Please, try again.'));
    }

    $this->set(compact('user'));
  }
*/


}
