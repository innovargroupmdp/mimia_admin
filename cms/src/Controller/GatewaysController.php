<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Gateways Controller
 *
 * @property \App\Model\Table\GatewaysTable $Gateways
 * @method \App\Model\Entity\Gateway[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GatewaysController extends AppController
{
  /**
   * Index method
   *
   * @return \Cake\Http\Response|null|void Renders view
   */
  public function index($id = null)
  {
    //TODO Add Models
    $this->Authorization->skipAuthorization();
    $this->paginate = [
        'contain' => ['Clients','Models'],
    ];


    /**
     * View method
     *
     * @param string|null $id Gateway id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gateway = $this->Gateways->get($id, [
            'contain' => ['Clients', 'Measures'],
        ]);

    $Models  = $this->fetchTable('Models')->find()->all();
    $models  = $this->keyvalue($Models);
    
    $this->viewBuilder()->setLayout('template_main');
    $this->set(compact('gateways','models'));
    
  }

  /**
   * View method
   *
   * @param string|null $id Gateway id.
   * @return \Cake\Http\Response|null|void Renders view
   * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
   */
  public function view($id = null)
  {
    $gateway = $this->Gateways->get($id, [
        'contain' => ['Clients'],
    ]);

    $this->set(compact('gateway'));
  }

    /**
     * Edit method
     *
     * @param string|null $id Gateway id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gateway = $this->Gateways->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gateway = $this->Gateways->patchEntity($gateway, $this->request->getData());
            if ($this->Gateways->save($gateway)) {
                $this->Flash->success(__('The gateway has been saved.'));

              return $this->redirect(['action' => 'index']);
          }
          $this->Flash->error(__('The gateway could not be saved. Please, try again.'));
      }
      $clients = $this->Gateways->Clients->find('list', ['limit' => 200])->all();
      $this->set(compact('gateway', 'clients'));
  }

  /**
   * Edit method
   *
   * @param string|null $id Gateway id.
   * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
   * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
   */
  public function edit($id = null)
  {
      $gateway = $this->Gateways->get($id, [
          'contain' => ['Clients'],
      ]);
      if ($this->request->is(['patch', 'post', 'put'])) {
          $gateway = $this->Gateways->patchEntity($gateway, $this->request->getData());
          if ($this->Gateways->save($gateway)) {
              $this->Flash->success(__('The gateway has been saved.'));

              return $this->redirect(['action' => 'index']);
          }
          $this->Flash->error(__('The gateway could not be saved. Please, try again.'));
      }
      $clients = $this->Gateways->Clients->find('list', ['limit' => 200])->all();
      $this->set(compact('gateway', 'clients'));
  }

  /**
   * Delete method
   *
   * @param string|null $id Gateway id.
   * @return \Cake\Http\Response|null|void Redirects to index.
   * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
   */
  public function delete($id = null)
  {
      $this->request->allowMethod(['post', 'delete']);
      $gateway = $this->Gateways->get($id);
      if ($this->Gateways->delete($gateway)) {
          $this->Flash->success(__('The gateway has been deleted.'));
      } else {
          $this->Flash->error(__('The gateway could not be deleted. Please, try again.'));
      }

      return $this->redirect(['action' => 'index']);
  }

  /**
   *                My Owm Code
   * 
   */

  /**
   * list method
   * Lists all gateways that below to client $id.
   *
   * @return \Cake\Http\Response|null|void Renders view
   */
  public function list($id = null)
  {
    //TODO Add Models
    $this->Authorization->skipAuthorization();
    $this->paginate = [
        'contain' => ['Clients',],
      ];
    
    $query     = $this->Gateways->find();
    $result    = $query->where( ['client_id =' => $id] );
    $gateways  = $this->paginate($result);

    $modelTypeId  = $this->fetchTable('DeviceTypes')->find('list')->where( ['type =' => 'gateway'] );

    $models = $this->fetchTable('Models')->find()->where( ['device_type_id =' => $modelTypeId->id] );

    $this->viewBuilder()->setLayout('template_main');   
    $this->set(compact('gateways','models',));
  }

}
