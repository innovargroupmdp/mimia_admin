<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Mimias Controller
 *
 * @property \App\Model\Table\MimiasTable $Mimias
 * @method \App\Model\Entity\Mimia[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MimiasController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Clients'],
        ];
        $mimias = $this->paginate($this->Mimias);

        $this->set(compact('mimias'));
    }

    /**
     * View method
     *
     * @param string|null $id Mimia id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mimia = $this->Mimias->get($id, [
            'contain' => ['Clients', 'Devices', 'Measures'],
        ]);

        $this->set(compact('mimia'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mimia = $this->Mimias->newEmptyEntity();
        if ($this->request->is('post')) {
            $mimia = $this->Mimias->patchEntity($mimia, $this->request->getData());
            if ($this->Mimias->save($mimia)) {
                $this->Flash->success(__('The mimia has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The mimia could not be saved. Please, try again.'));
        }
        $clients = $this->Mimias->Clients->find('list', ['limit' => 200])->all();
        $this->set(compact('mimia', 'clients'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Mimia id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mimia = $this->Mimias->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mimia = $this->Mimias->patchEntity($mimia, $this->request->getData());
            if ($this->Mimias->save($mimia)) {
                $this->Flash->success(__('The mimia has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The mimia could not be saved. Please, try again.'));
        }
        $clients = $this->Mimias->Clients->find('list', ['limit' => 200])->all();
        $this->set(compact('mimia', 'clients'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Mimia id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mimia = $this->Mimias->get($id);
        if ($this->Mimias->delete($mimia)) {
            $this->Flash->success(__('The mimia has been deleted.'));
        } else {
            $this->Flash->error(__('The mimia could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
