<?php 

use Cake\Event\Event;

trait UsersControllerExt{

/**
 * beforeFilter
 * enhable auth methods besides auth method in AppControler.php
 * deny everything lest login.
 *
 */
public function beforeFilter(Event $event )
{
     $this->Auth->allow(['singup','forgotPassword']);
}

/**
 * forgotPassword method
 *
 * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
 */
public function forgotPassword()
{
    //
}

/**
 * login method
 *
 * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
 */
public function login()
{
  
  if ($this->request->is(['post','put'])) {
    $user = $this->Auth->identify();
    if ($user) {
      $this->Flash->success(__('The user is correct.'));
      $this->user->setUser();
      //redirect
      return $this->redirect(['Controller'=>'Users','action'=>'index']);
    }
    $this->Flash->error(__('The user or password could not be checked. Please, try again.'));
  }

  $this->set(compact('user'));
}

/**
 * singup method
 *
 * @param string|null $id User id.
 * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
public function singup($id = null)
{
    $user = $this->Users->get($id, [
        'contain' => [],
    ]);
    if ($this->request->is(['patch', 'post', 'put'])) {
        $user = $this->Users->patchEntity($user, $this->request->getData());
        if ($this->Users->save($user)) {
            $this->Flash->success(__('The user has been saved.'));

            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('The user could not be saved. Please, try again.'));
    }
    $roles = $this->Users->Roles->find('list', ['limit' => 200])->all();
    $clients = $this->Users->Clients->find('list', ['limit' => 200])->all();
    $this->set(compact('user', 'roles', 'clients'));
}


}
?>    