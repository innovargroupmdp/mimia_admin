<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\ORM\Locator\LocatorAwareTrait;   

/**
 * Clients Controller
 *
 * @property \App\Model\Table\ClientsTable $Clients
 * @method \App\Model\Entity\Client[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
*/

class ClientsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->Authorization->skipAuthorization();
        $this->paginate = [
            'contain' => ['CompanyTypes', 'Plans', 'Billings', 'States'],
        ];
        $clients = $this->paginate($this->Clients);
        
        $CompanyTypes = $this->Clients->CompanyTypes->find()->all();
        $Plans        = $this->Clients->Plans->find()->all();
        $States       = $this->Clients->States->find()->all();   

        $companyType  = $this->keyvalue($CompanyTypes);
        $plans        = $this->keyvalue($Plans);
        $states       = $this->keyvalue($States);

        $this->viewBuilder()->setLayout('template_main');
        $this->set(compact('clients','companyType','plans','states'));
    }

    /**
     * View method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
      
      $client = $this->Clients->get($id, [
          'contain' => ['CompanyTypes', 'Plans', 'Billings', 'States','Users',],
      ]);
      $this->Authorization->authorize($client);
      $this->viewBuilder()->setLayout('template_main');

      $companyTypes = $this->Clients->CompanyTypes->find('list')->all();
      $plans        = $this->Clients->Plans->find('list')->all();
      $billings     = $this->Clients->Billings->find('list')->all();
      $states       = $this->Clients->States->find('list')->all();
      

      $entity       =  $this->fetchTable('Users');
      $query        = $entity->find();
      //seach all users for this client.
      $Users        = $query->where(['client_id ='=> $id]); 
            
      $this->paginate = [
            'contain' => ['Roles',],
        ];
      $users = $this->paginate($Users);      
 
      $this->set(compact('client', 'companyTypes', 'plans', 'billings', 'states','users'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
      $this->Authorization->skipAuthorization();    
      $client = $this->Clients->newEmptyEntity();
      if ($this->request->is('post')) {
          $client = $this->Clients->patchEntity($client, $this->request->getData());
          if ($this->Clients->save($client)) {
              $this->Flash->success(__('The client has been saved.'));

              return $this->redirect(['action' => 'index']);
          }
          $this->Flash->error(__('The client could not be saved. Please, try again.'));
      }
      $companyTypes = $this->Clients->CompanyTypes->find('list', ['limit' => 200])->all();
      $plans        = $this->Clients->Plans->find('list', ['limit' => 200])->all();
      $billings     = $this->Clients->Billings->find('list', ['limit' => 200])->all();
      $states       = $this->Clients->States->find('list', ['limit' => 200])->all();
      $this->set(compact('client', 'companyTypes', 'plans', 'billings', 'states'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
      $this->Authorization->skipAuthorization();
      $client = $this->Clients->get($id, [
          'contain' => [],
      ]);
      if ($this->request->is(['patch', 'post', 'put'])) {
          $client = $this->Clients->patchEntity($client, $this->request->getData());
          if ($this->Clients->save($client)) {
              $this->Flash->success(__('The client has been saved.'));

              return $this->redirect(['action' => 'view']);
          }
          $this->Flash->error(__('The client could not be saved. Please, try again.'));
      }
        $companyTypes = $this->Clients->CompanyTypes->find('list', ['limit' => 200])->all();
        $plans = $this->Clients->Plans->find('list', ['limit' => 200])->all();
        $billings = $this->Clients->Billings->find('list', ['limit' => 200])->all();
        $states = $this->Clients->States->find('list', ['limit' => 200])->all();
        $this->set(compact('client', 'companyTypes', 'plans', 'billings', 'states'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
      $this->request->allowMethod(['post', 'delete']);
      $client = $this->Clients->get($id);
      if ($this->Clients->delete($client)) {
          $this->Flash->success(__('The client has been deleted.'));
      } else {
          $this->Flash->error(__('The client could not be deleted. Please, try again.'));
      }

      return $this->redirect(['action' => 'index']);
    }

    
}
