<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Client;
use Authorization\IdentityInterface;

/**
 * Clients policy
 */
class ClientPolicy
{
    /**
     * Check if $client can add Clients
     *return true;
     * @param \Authorization\IdentityInterface $client The client.
     * @param \App\Model\Entity\Clients $clients
     * @return bool
     */
    public function canAdd(IdentityInterface $client, Client $clients)
    {
        return true;
    }

    /**
     * Check if $client can edit Clients
     *
     * @param \Authorization\IdentityInterface $client The client.
     * @param \App\Model\Entity\Clients $clients
     * @return bool
     */
    public function canEdit(IdentityInterface $client, Client $clients)
    {
        return true;
    }

    /**
     * Check if $client can delete Clients
     *
     * @param \Authorization\IdentityInterface $client The client.
     * @param \App\Model\Entity\Clients $clients
     * @return bool
     */
    public function canDelete(IdentityInterface $client, Client $clients)
    {
        return true;
    }

    /**
     * Check if $client can view Clients
     *
     * @param \Authorization\IdentityInterface $client The client.
     * @param \App\Model\Entity\Clients $clients
     * @return bool
     */
    public function canView(IdentityInterface $client, Client $clients)
    {
        return true;
    }
/**
   *                  My Owm Code
   * 
   */

  /**
   * can view clients
   *
   * @param \Authorization\IdentityInterface $client The client.
   * @param \App\Model\Entity\client $clients
   * @return bool
   */

  public function canIndex(IdentityInterface $client, Client $clients)
  {
      return true;
  }

  public function canHome(IdentityInterface $client, Client $clients)
  {
      return true;
  }
  
}




