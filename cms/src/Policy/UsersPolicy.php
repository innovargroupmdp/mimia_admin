<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\User;
use App\Policy\UserPolicy;
use Authorization\IdentityInterface;

/**
 * Users policy
 */
class UsersPolicy extends UserPolicy
{
    
}
