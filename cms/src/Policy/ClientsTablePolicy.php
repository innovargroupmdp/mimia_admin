<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Table\ClientsTable;
use Authorization\IdentityInterface;

/**
 * Clients policy
 */
class ClientsTablePolicy
{
  public function scopeIndex($client, $query)
  {
    return $query->where(['Clients.client_id' => $client->getIdentifier()]);
  }
}
