-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 20-10-2022 a las 22:42:33
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mimia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients`
--

CREATE TABLE `clients` (
  `id` int(6) UNSIGNED NOT NULL,
  `client_tag` int(6) UNSIGNED NOT NULL,
  `user` char(255) DEFAULT NULL,
  `password` char(255) DEFAULT NULL,
  `name` char(255) NOT NULL,
  `cuit` char(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `act` tinyint(1) NOT NULL DEFAULT 0,
  `enroll` datetime NOT NULL DEFAULT current_timestamp(),
  `down` datetime DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_controls`
--

CREATE TABLE `data_controls` (
  `id` int(11) UNSIGNED NOT NULL,
  `subtype` tinyint(4) UNSIGNED NOT NULL,
  `data_ctrl_file` char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'nombre de archivo  que contiene datos formato json',
  `rx_tx` tinyint(1) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `device_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `devices`
--

CREATE TABLE `devices` (
  `id` int(11) UNSIGNED NOT NULL,
  `dvc_tag` int(6) UNSIGNED NOT NULL,
  `tk_mm_sr` int(6) UNSIGNED NOT NULL,
  `tk_mm` int(6) UNSIGNED NOT NULL,
  `act` tinyint(1) NOT NULL DEFAULT 0,
  `boot` tinyint(1) NOT NULL DEFAULT 0,
  `mimia_id` int(11) UNSIGNED NOT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gateways`
--

CREATE TABLE `gateways` (
  `id` int(11) UNSIGNED NOT NULL,
  `gw_tag` int(6) UNSIGNED NOT NULL,
  `gw_mac` int(6) UNSIGNED NOT NULL COMMENT 'MAC serie',
  `mac` char(23) NOT NULL,
  `key_svr0` char(11) NOT NULL COMMENT 'pass de fábrica',
  `key_svr` char(11) DEFAULT NULL COMMENT 'Pass entregada luego de la asociaciòn\r\n',
  `boot` tinyint(1) NOT NULL DEFAULT 0,
  `boot_Init` tinyint(1) NOT NULL DEFAULT 0,
  `release_version` char(10) NOT NULL COMMENT 'Versión de firmaware de dispositivo',
  `act` tinyint(1) NOT NULL DEFAULT 0,
  `on_service` date DEFAULT NULL,
  `off_service` date DEFAULT NULL,
  `client_id` int(6) UNSIGNED NOT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='tkSrv0';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `measures`
--

CREATE TABLE `measures` (
  `id` int(11) UNSIGNED NOT NULL,
  `slot` smallint(6) UNSIGNED NOT NULL,
  `trie_n` smallint(6) UNSIGNED NOT NULL,
  `tx_power` tinyint(4) UNSIGNED NOT NULL,
  `noise_lv` tinyint(4) UNSIGNED NOT NULL,
  `rx_signal` tinyint(4) UNSIGNED NOT NULL,
  `battery_lv` decimal(2,1) UNSIGNED NOT NULL,
  `current_tx_date` time NOT NULL,
  `current_measure` mediumint(9) UNSIGNED NOT NULL,
  `current_measure_date` date NOT NULL,
  `measure_dif_file` char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Nombre de Archivo',
  `mimia_id` int(11) UNSIGNED NOT NULL,
  `gateway_id` int(11) UNSIGNED NOT NULL COMMENT 'util para ver cambios en la red',
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='measuresDif';

--
-- Volcado de datos para la tabla `measures`
--

INSERT INTO `measures` (`id`, `slot`, `trie_n`, `tx_power`, `noise_lv`, `rx_signal`, `battery_lv`, `current_tx_date`, `current_measure`, `current_measure_date`, `measure_dif_file`, `mimia_id`, `gateway_id`, `comment`) VALUES
(1, 0, 0, 0, 0, 0, '0.0', '11:12:01', 1, '2021-01-10', '{\"Index\":3,\"MedidaD\":[1,3,2]}', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mimias`
--

CREATE TABLE `mimias` (
  `id` int(11) UNSIGNED NOT NULL,
  `mimia_tag` int(6) UNSIGNED NOT NULL,
  `mm_mac` int(6) UNSIGNED NOT NULL,
  `mac` char(23) NOT NULL,
  `mm_tk_0` int(6) UNSIGNED NOT NULL,
  `slot` smallint(6) UNSIGNED NOT NULL,
  `boot` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Booteo final, el dispositivo se autentico luego de la asociación',
  `boot_init` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Booteo inicial, no implica  recepcion del mimia.',
  `release_version` char(255) NOT NULL COMMENT 'Versión firmware',
  `act` tinyint(1) NOT NULL DEFAULT 0,
  `on_service` date DEFAULT NULL,
  `off_service` date DEFAULT NULL,
  `client_id` int(6) UNSIGNED NOT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(2) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `attribute` int(2) UNSIGNED DEFAULT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `attribute`, `comment`) VALUES
(1, 'Admin', 10, 'General manager of all system platform. Super User.'),
(2, 'Moderator', 3, 'Staff manager for this platform.'),
(3, 'User', 1, 'General client of service'),
(4, 'Support', 5, 'Staff tecnical of Swica.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `user` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(2) UNSIGNED NOT NULL,
  `client_id` int(11) UNSIGNED NOT NULL,
  `act` tinyint(1) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idClient` (`client_tag`),
  ADD UNIQUE KEY `client_tag` (`client_tag`),
  ADD UNIQUE KEY `user` (`user`);

--
-- Indices de la tabla `data_controls`
--
ALTER TABLE `data_controls`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `mimia_id` (`mimia_id`);

--
-- Indices de la tabla `gateways`
--
ALTER TABLE `gateways`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `MAC` (`mac`),
  ADD KEY `client_id` (`client_id`);

--
-- Indices de la tabla `measures`
--
ALTER TABLE `measures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mimia_id` (`mimia_id`);

--
-- Indices de la tabla `mimias`
--
ALTER TABLE `mimias`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `MAC` (`mac`),
  ADD KEY `client_id` (`client_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user` (`user`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `client_id` (`client_id`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `data_controls`
--
ALTER TABLE `data_controls`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `gateways`
--
ALTER TABLE `gateways`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `measures`
--
ALTER TABLE `measures`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `mimias`
--
ALTER TABLE `mimias`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
